#/*
# * Copyright © 2018, 2019 Maël A
# *
# * This file is part of CLEP, a leaves scheduler web app.
# *
# * CLEP is free software: you can redistribute it and/or modify
# * it under the terms of the GNU Affero General Public License as
# * published by the Free Software Foundation, either version 3 of the
# * License, or (at your option) any later version.
# *
# * CLEP is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Affero General Public License for more details.
# *
# * You should have received a copy of the GNU Affero General Public License
# * along with CLEP.  If not, see <https://www.gnu.org/licenses/>.
# */

fields_pos = {
    "current_year": {"places": ["A1"]},
    "request_date": {"places": ["A2"]},
    "employee_name": {"places": ["A3"]},
    "functions_label": {"places": ["A4"]},
    "functions": {"places": ["A5"]},
    "total_leaves": {"places": ["A6"]},
    "leaves_used": {"places": ["A7"]},
    "leaves_left": {"places": ["A8"]},
    "count_requested": {"places": ["A9"]},
    "left_after": {"places": ["A10"]},
    "left_last_year": {"places": ["A11"]},
    "previous_year": {"places": ["A12"]},
    "periods": {"places": ["A13"]},
    "last_year": {"places": ["A14"]},
    "previous_year_after": {"places": ["A15"]}
}

style_name = "ce1"
