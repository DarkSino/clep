#/*
# * Copyright © 2018, 2019 Maël A
# *
# * This file is part of CLEP, a leaves scheduler web app.
# *
# * CLEP is free software: you can redistribute it and/or modify
# * it under the terms of the GNU Affero General Public License as
# * published by the Free Software Foundation, either version 3 of the
# * License, or (at your option) any later version.
# *
# * CLEP is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Affero General Public License for more details.
# *
# * You should have received a copy of the GNU Affero General Public License
# * along with CLEP.  If not, see <https://www.gnu.org/licenses/>.
# */
#
#/*
# * This file and CLEP copy code from and use the Django web framework which is
# * distributed under the following license:
# *
# *
# * Copyright (c) Django Software Foundation and individual contributors.
# * All rights reserved.
# *
# * Redistribution and use in source and binary forms, with or without
# * modification, are permitted provided that the following conditions are met:
# *
# *     1. Redistributions of source code must retain the above copyright
# *        notice, this list of conditions and the following disclaimer.
# *
# *     2. Redistributions in binary form must reproduce the above copyright
# *        notice, this list of conditions and the following disclaimer in the
# *        documentation and/or other materials provided with the distribution.
# *
# *     3. Neither the name of Django nor the names of its contributors may be
# *        used to endorse or promote products derived from this software
# *        without specific prior written permission.
# *
# * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# * POSSIBILITY OF SUCH DAMAGE.
# */

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import get_user_model
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.utils import timezone as django_timezone
from django.urls import reverse
from django.template.defaultfilters import date as django_date
from django.template.defaultfilters import capfirst
from django.forms import formset_factory
from django.core.exceptions import PermissionDenied

from datetime import date, datetime, timedelta
from calendar import monthrange
from pytz import timezone

from planning import models
from .modules.models_helper import (
    get_leave_nonattendancetype,
    get_blank_color_setting
)
from .modules.misc import get_current_year
from .modules.employee import get_display_name
from .utils.calendar_view import (
    get_user_data,
    sort_users_by_families,
    get_user_color
)
from .utils.leave_request_view import (add_metadata, make_leave_request,
                                                        modify_leave_request)
from .utils.requests_list_view import (
    make_requests_lines,
    get_headers_sort_context,
    get_filters_context,
    filter_requests,
    get_leave_request_sheet
)
from .utils.common import (
    get_grouped_nonattendances,
    delete_leave_requests,
    accept_leave_requests,
    unaccept_leave_requests,
    leaves_left
)
from .utils.modify_request_view import convert_to_form_data
from .utils.add_nonattendance_view import (
    make_nonattendance,
    modify_nonattendance
)
from .utils.report_views import get_report_context
from .utils.set_oncall_week_employee_view import (
    delete_oncall_week,
    set_oncall_week
)
from .utils.place_regular_na_view import (place_regular_nonattendance,
                                    place_employee_regular_nonattendances)
from .utils.add_leave_request_view import make_leaverequest

from . import strings, urls, forms

ADMIN_GET_PARAMS = "?o=-2&f=1.4-0.2"
USER_GET_PARAMS = "?f=0.4"

def index(request):
    now = django_timezone.now().astimezone(timezone("Europe/Paris"))
    return HttpResponseRedirect(
        reverse("planning:calendar", args=(now.year, now.month))
    )

def calendar(request, year, month):
    show_nonvalidated_message(request)
    User = get_user_model()
    month_start = date(year, month, 1)
    if month != 12:
        next_month_start = date(year, month + 1, 1)
    else:
        next_month_start = date(year + 1, 1, 1)
    users = list(User.objects.exclude(leftdate__date__lte=month_start).exclude(
                joineddate__date__gte=next_month_start).exclude(username="root"
                                                        ).order_by("last_name"))
    sort_users_by_families(users)
    users_data = []
    alternate_display = models.Setting.objects.get(
                                name="affichage_alternatif").value == "True"
    for user in users:
        users_data.append(get_user_data(user, year, month, alternate_display))
    dayparts = [daypart.name for daypart in models.DayPart.objects \
                                                            .order_by("pk")]
    if alternate_display:
        rows_per_user = range(len(dayparts))
    else:
        rows_per_user = range(2)
    now = django_timezone.now().astimezone(timezone("Europe/Paris"))
    is_index = now.year == year and now.month == month
    days = []
    for day_number in range(1, monthrange(year, month)[1] + 1):
        days.append(django_date(date(year, month, day_number), "D"))
    settings_names = ["couleur_astreinte", "couleur_fériés", "couleur_weekend"]
    colors_names = ["astreinte", "fériés", "week-end"]
    colors_descs = []
    for color_def in zip(settings_names, colors_names):
        setting = models.Setting.objects.get(name=color_def[0])
        colors_descs.append({"name": color_def[1], "value": setting.value})
    show_weeks_number = models.Setting.objects.get(
                        name="afficher_le_numero_des_semaines").value == "True"
    if show_weeks_number:
        weeks = get_weeks_number_data(year, month, len(dayparts))
    else:
        weeks = None
    context = {
        "users": users_data,
        "days": days,
        "show_days_name": models.Setting.objects.get(
                            name="afficher_le_nom_des_jours").value == "True",
        "weeks": weeks,
        "show_weeks_number": show_weeks_number,
        "dayparts": dayparts,
        "daypart_count": len(dayparts),
        "alternate_display": alternate_display,
        "rows_per_user": rows_per_user,
        "title": capfirst(django_date(month_start, 'F Y')),
        "is_index": is_index,
        "nonattendances_types": models.NonattendanceType.objects.all(),
        "colors": colors_descs,
    }
    context.update(get_common_context(request))
    request.current_app = urls.app_name
    return render(request, "planning/calendar.html", context)

def append_week_data(weeks_data, week_data, daypart_count):
    week_data["length"] = week_data["day_count"] * daypart_count
    del week_data["day_count"]
    weeks_data.append(week_data)

def get_weeks_number_data(year, month, daypart_count):
    weeks_data = []
    day_date = date(year, month, 1)
    week_data = {"number": day_date.isocalendar()[1],"day_count": 1}
    for i in range(2, monthrange(year, month)[1] + 1):
        day_date = day_date.replace(day=i)
        if day_date.isocalendar()[1] == week_data["number"]:
            week_data["day_count"] += 1
            continue
        append_week_data(weeks_data, week_data, daypart_count)
        week_data = {"number": day_date.isocalendar()[1],"day_count": 1}
    append_week_data(weeks_data, week_data, daypart_count)
    return weeks_data

def get_common_context(request):
    return {
        "site_title": strings.site_title,
        "site_header": strings.site_header,
        "site_url": None
    }

def login(request):
    extra_context = dict(
        get_common_context(request),
        title=strings.login_page_title,
        app_path=request.get_full_path(),
        username=request.user.get_username()
    )
    request.current_app = urls.app_name
    return LoginView.as_view(
        template_name="planning/login.html",
        authentication_form=forms.LeavesAuthenticationForm,
        extra_context=extra_context
    )(request)

def logout(request):
    request.current_app = urls.app_name
    return LogoutView.as_view(template_name="planning/empty.html",
                                            next_page="planning:index")(request)

def change_password(request):
    show_nonvalidated_message(request)
    extra_context = dict(
        get_common_context(request),
        submit_text=strings.modify_password
    )
    request.current_app = urls.app_name
    return PasswordChangeView.as_view(
        template_name="planning/generic_form.html",
        success_url=reverse("planning:password_change_done"),
        extra_context=extra_context
    )(request)

def password_change_done(request):
    messages.add_message(request, messages.SUCCESS, strings.password_changed)
    return HttpResponseRedirect(reverse("planning:index"))

def calendar_history(request):
    show_nonvalidated_message(request)
    context = {}
    if request.method == "POST":
        form = forms.CalendarHistoryForm(request.POST)
        if form.is_valid():
            date = form.cleaned_data["period"]
            return HttpResponseRedirect(
                reverse("planning:calendar", args=(date.year, date.month))
            )
        else:
            context.update({
                "errors": form.errors.values
            })
    else:
        form = forms.CalendarHistoryForm()
    context.update({
        "form_url": request.path,
        "form": form,
        "submit_text": strings.ok,
        "page_name": strings.calendar_history_page_name,
        "title": strings.calendar_history_title
    })
    context.update(get_common_context(request))
    request.current_app = urls.app_name
    return render(request, "planning/generic_form.html", context)

@login_required
def leave_request(request):
    show_nonvalidated_message(request)
    context = {}
    PeriodNonattendanceFormSet = formset_factory(forms.PeriodNonattendanceForm,
                                                                        extra=0)
    DayNonattendanceFormSet = formset_factory(forms.DayNonattendanceForm,
                                                                        extra=0)
    DayPartNonattendanceFormSet = formset_factory(
                                        forms.DayPartNonattendanceForm, extra=0)
    if request.method == "POST":
        period_formset = PeriodNonattendanceFormSet(request.POST,
                                                            prefix="p-period")
        day_formset = DayNonattendanceFormSet(request.POST, prefix="p-day")
        daypart_formset = DayPartNonattendanceFormSet(request.POST,
                                                            prefix="p-daypart")
        if period_formset.is_valid() and day_formset.is_valid() \
                                                and daypart_formset.is_valid():
            if make_leave_request(request.user, request, period_formset,
                                                day_formset, daypart_formset):
                messages.add_message(request, messages.SUCCESS,
                                                strings.leave_request_success)
                return HttpResponseRedirect(reverse("planning:index"))
        errors_values = []
        for formset in [period_formset, day_formset, daypart_formset]:
            for errors_dict in formset.errors:
                for value in errors_dict.values():
                    errors_values.append(value)
        context.update({
            "errors": errors_values
        })
    else:
        period_formset = PeriodNonattendanceFormSet(prefix="p-period")
        day_formset = DayNonattendanceFormSet(prefix="p-day")
        daypart_formset = DayPartNonattendanceFormSet(prefix="p-daypart")
    add_metadata(period_formset, strings.period_formset_title,
                                    strings.period_formset_button)
    add_metadata(day_formset, strings.day_formset_title,
                                            strings.day_formset_button)
    add_metadata(daypart_formset, strings.daypart_formset_title,
                                strings.daypart_formset_button)
    context.update({
        "form_url": request.path,
        "inline_formsets": [period_formset, day_formset, daypart_formset],
        "submit_text": strings.leave_request_submit,
        "page_name": strings.leave_request_page_name,
        "title": strings.leave_request_title
    })
    context.update(get_common_context(request))
    request.current_app = urls.app_name
    return render(request, "planning/generic_form.html", context)

@login_required
def requests_list(request):
    return HttpResponseRedirect(
        reverse("planning:requestslistbackend") + USER_GET_PARAMS
    )

@login_required
def requests_list_backend(request):
    show_nonvalidated_message(request)
    headers_title = [
        strings.request_list_header_1,
        strings.request_list_header_2,
        strings.request_list_header_3,
        strings.request_list_header_4,
    ]
    leave_type = get_leave_nonattendancetype()
    requests = models.LeaveRequest.objects.filter(
            nonattendance__employee=request.user,
                                nonattendance__type=leave_type).distinct()
    requests = filter_requests(requests, request)
    if request.method == "POST":
        form = forms.RequestsListActionsForm(request.POST, user=request.user,
                                                            queryset=requests)
        if form.is_valid():
            if form.cleaned_data["action"] == "nop":
                messages.add_message(request, messages.WARNING,
                                                            strings.no_action)
                return HttpResponseRedirect(reverse("planning:requestslist"))
            elif form.cleaned_data["action"] == "modify":
                if form.cleaned_data["requests"].count() != 1:
                    messages.add_message(request, messages.ERROR,
                                                    strings.only_one_request)
                    return HttpResponseRedirect(
                                            reverse("planning:requestslist"))
                elif form.cleaned_data["requests"][0].validated:
                    messages.add_message(request, messages.ERROR,
                                                    strings.already_validated)
                    return HttpResponseRedirect(
                                            reverse("planning:requestslist"))
                return HttpResponseRedirect(reverse("planning:modifyrequest",
                                args=(form.cleaned_data["requests"][0].pk,)))
            elif form.cleaned_data["action"] == "download_sheet":
                if form.cleaned_data["requests"].count() != 1:
                    messages.add_message(request, messages.ERROR,
                                                    strings.only_one_download)
                    return HttpResponseRedirect(
                                            reverse("planning:requestslist"))
                leave_request = form.cleaned_data["requests"][0]
                ods_doc = get_leave_request_sheet(leave_request)
                response = HttpResponse(ods_doc.tobytes(),
                                                content_type=ods_doc.mimetype)
                fr_tz = timezone("Europe/Paris")
                local_date = leave_request.date.astimezone(fr_tz)
                date_str = local_date.isoformat(timespec="seconds")
                filename = str(leave_request.nonattendance_set.all()[0] \
                                .employee.last_name) + "-" + date_str + ".ods"
                response['Content-Disposition'] = \
                                    "attachment; filename=\"" + filename + "\""
                return response
            else: #actions: delete
                delete_leave_requests(request, form.cleaned_data["requests"])
                return HttpResponseRedirect(reverse("planning:requestslist"))
        else:
            if form.errors["requests"][0] == "Ce champ est obligatoire.":
                messages.add_message(request, messages.WARNING,
                                                            strings.no_request)
    else:
        form = forms.RequestsListActionsForm(user=request.user,
                                                            queryset=requests)
    requests_lines = make_requests_lines(form.queryset, form, request)
    context = {
        "title": strings.request_list_page_title,
        "title_header": strings.request_list_header_title,
        "lines": requests_lines,
        "form": form,
        "entry_count": len(form.queryset)
    }
    context.update(get_common_context(request))
    context.update(get_headers_sort_context(headers_title, request))
    context.update(get_filters_context(request))
    request.current_app = urls.app_name
    return render(request, "planning/requests_list.html", context)

@login_required
def modify_request(request, request_id):
    show_nonvalidated_message(request)
    leave_request = models.LeaveRequest.objects.get(pk=request_id)
    leave_request_owner = leave_request.nonattendance_set.all()[0].employee
    if request.user != leave_request_owner:
        return HttpResponseRedirect(reverse("planning:login") + "?next=" +
                        reverse("planning:modifyrequest", args=(request_id,)))
    if leave_request.validated:
        raise PermissionDenied
    context = {}
    initial_data = get_grouped_nonattendances(leave_request)
    initial_data = convert_to_form_data(initial_data)
    PeriodNonattendanceFormSet = formset_factory(forms.PeriodNonattendanceForm,
                                                    extra=0, can_delete=True)
    DayNonattendanceFormSet = formset_factory(forms.DayNonattendanceForm,
                                                    extra=0, can_delete=True)
    DayPartNonattendanceFormSet = formset_factory(
                    forms.DayPartNonattendanceForm, extra=0, can_delete=True)
    if request.method == "POST":
        period_formset = PeriodNonattendanceFormSet(request.POST,
                            initial=initial_data["periods"], prefix="p-period")
        day_formset = DayNonattendanceFormSet(request.POST,
                                initial=initial_data["days"], prefix="p-day")
        daypart_formset = DayPartNonattendanceFormSet(request.POST,
                            initial=initial_data["parts"], prefix="p-daypart")
        if period_formset.is_valid() and day_formset.is_valid() \
                                                and daypart_formset.is_valid():
            if modify_leave_request(request.user, request, leave_request,
                                period_formset, day_formset, daypart_formset):
                messages.add_message(request, messages.SUCCESS,
                                        strings.leave_request_modify_success)
                return HttpResponseRedirect(reverse("planning:requestslist"))
        errors_values = []
        for formset in [period_formset, day_formset, daypart_formset]:
            for errors_dict in formset.errors:
                for value in errors_dict.values():
                    errors_values.append(value)
        context.update({
            "errors": errors_values
        })
    else:
        period_formset = PeriodNonattendanceFormSet(prefix="p-period",
                                                initial=initial_data["periods"])
        day_formset = DayNonattendanceFormSet(prefix="p-day",
                                                initial=initial_data["days"])
        daypart_formset = DayPartNonattendanceFormSet(prefix="p-daypart",
                                                initial=initial_data["parts"])
    add_metadata(period_formset, strings.period_formset_title,
                                    strings.period_formset_button)
    add_metadata(day_formset, strings.day_formset_title,
                                        strings.day_formset_button)
    add_metadata(daypart_formset, strings.daypart_formset_title,
                                strings.daypart_formset_button)
    context.update({
        "form_url": request.path,
        "inline_formsets": [period_formset, day_formset, daypart_formset],
        "submit_text": strings.modify_request_submit,
        "page_name": strings.modify_request_page_name,
        "title": strings.modify_request_page_title
    })
    context.update(get_common_context(request))
    request.current_app = urls.app_name
    return render(request, "planning/generic_form.html", context)

def staff_check(user):
    return user.is_staff

def can_modify_request(user):
    return user.has_perm("planning.modify_request")

@user_passes_test(can_modify_request)
def admin_modify_request(request, request_id):
    show_nonvalidated_message(request)
    leave_request = models.LeaveRequest.objects.get(pk=request_id)
    leave_request_owner = leave_request.nonattendance_set.all()[0].employee
    context = {}
    initial_data = get_grouped_nonattendances(leave_request)
    initial_data = convert_to_form_data(initial_data)
    PeriodNonattendanceFormSet = formset_factory(forms.PeriodNonattendanceForm,
                                                    extra=0, can_delete=True)
    DayNonattendanceFormSet = formset_factory(forms.DayNonattendanceForm,
                                                    extra=0, can_delete=True)
    DayPartNonattendanceFormSet = formset_factory(
                    forms.DayPartNonattendanceForm, extra=0, can_delete=True)
    if request.method == "POST":
        period_formset = PeriodNonattendanceFormSet(request.POST,
                            initial=initial_data["periods"], prefix="p-period")
        day_formset = DayNonattendanceFormSet(request.POST,
                                initial=initial_data["days"], prefix="p-day")
        daypart_formset = DayPartNonattendanceFormSet(request.POST,
                            initial=initial_data["parts"], prefix="p-daypart")
        if period_formset.is_valid() and day_formset.is_valid() \
                                                and daypart_formset.is_valid():
            past_allowed = request.user.is_staff
            if modify_leave_request(leave_request_owner, request, leave_request,
                    period_formset, day_formset, daypart_formset, past_allowed):
                messages.add_message(request, messages.SUCCESS,
                                        strings.leave_request_modify_success)
                return HttpResponseRedirect(
                                        reverse("planning:adminrequestslist"))
        errors_values = []
        for formset in [period_formset, day_formset, daypart_formset]:
            for errors_dict in formset.errors:
                for value in errors_dict.values():
                    errors_values.append(value)
        context.update({
            "errors": errors_values
        })
    else:
        period_formset = PeriodNonattendanceFormSet(prefix="p-period",
                                                initial=initial_data["periods"])
        day_formset = DayNonattendanceFormSet(prefix="p-day",
                                                initial=initial_data["days"])
        daypart_formset = DayPartNonattendanceFormSet(prefix="p-daypart",
                                                initial=initial_data["parts"])
    add_metadata(period_formset, strings.period_formset_title,
                                    strings.period_formset_button)
    add_metadata(day_formset, strings.day_formset_title,
                                            strings.day_formset_button)
    add_metadata(daypart_formset, strings.daypart_formset_title,
                                strings.daypart_formset_button)
    context.update({
        "form_url": request.path,
        "inline_formsets": [period_formset, day_formset, daypart_formset],
        "submit_text": strings.modify_request_submit,
        "page_name": strings.modify_request_page_name,
        "title": strings.modify_request_page_title
    })
    employee_name = leave_request_owner.first_name + " " + \
                                            leave_request_owner.last_name
    context.update({
        "extra_infos": [
            {"key": strings.admin_modify_employee_text, "value": employee_name}
        ]
    })
    context.update(get_common_context(request))
    request.current_app = urls.app_name
    return render(request, "planning/generic_form.html", context)

def can_modify_nonattendances(user):
    return user.has_perm("planning.modify_non_attendance")

@user_passes_test(can_modify_nonattendances)
def admin_modify_nonattendance(request, request_id):
    show_nonvalidated_message(request)
    leave_request = models.LeaveRequest.objects.get(pk=request_id)
    leave_request_owner = leave_request.nonattendance_set.all()[0].employee
    context = {}
    initial_data = get_grouped_nonattendances(leave_request)
    initial_data = convert_to_form_data(initial_data)
    PeriodNonattendanceFormSet = formset_factory(forms.PeriodNonattendanceForm,
                                                    extra=0, can_delete=True)
    DayNonattendanceFormSet = formset_factory(forms.DayNonattendanceForm,
                                                    extra=0, can_delete=True)
    DayPartNonattendanceFormSet = formset_factory(
                    forms.DayPartNonattendanceForm, extra=0, can_delete=True)
    if request.method == "POST":
        period_formset = PeriodNonattendanceFormSet(request.POST,
                            initial=initial_data["periods"], prefix="p-period")
        day_formset = DayNonattendanceFormSet(request.POST,
                                initial=initial_data["days"], prefix="p-day")
        daypart_formset = DayPartNonattendanceFormSet(request.POST,
                            initial=initial_data["parts"], prefix="p-daypart")
        if period_formset.is_valid() and day_formset.is_valid() \
                                                and daypart_formset.is_valid():
            if modify_nonattendance(request, leave_request,
                            period_formset, day_formset, daypart_formset, True):
                messages.add_message(request, messages.SUCCESS,
                                        strings.leave_request_modify_success)
                return HttpResponseRedirect(
                                        reverse("planning:nonattendanceslist"))
        errors_values = []
        for formset in [period_formset, day_formset, daypart_formset]:
            for errors_dict in formset.errors:
                for value in errors_dict.values():
                    errors_values.append(value)
        context.update({
            "errors": errors_values
        })
    else:
        period_formset = PeriodNonattendanceFormSet(prefix="p-period",
                                                initial=initial_data["periods"])
        day_formset = DayNonattendanceFormSet(prefix="p-day",
                                                initial=initial_data["days"])
        daypart_formset = DayPartNonattendanceFormSet(prefix="p-daypart",
                                                initial=initial_data["parts"])
    add_metadata(period_formset, strings.period_formset_title,
                                    strings.period_formset_button)
    add_metadata(day_formset, strings.day_formset_title,
                                            strings.period_formset_button)
    add_metadata(daypart_formset, strings.daypart_formset_title,
                                strings.daypart_formset_button)
    context.update({
        "form_url": request.path,
        "inline_formsets": [period_formset, day_formset, daypart_formset],
        "submit_text": strings.modify_nonattendance_submit,
        "page_name": strings.modify_nonattendance_page_name,
        "title": strings.modify_nonattendance_title
    })
    employee_name = leave_request_owner.first_name + " " + \
                                            leave_request_owner.last_name
    context.update({
        "extra_infos": [
            {"key": strings.modify_nonattendance_employee_text,
                                                        "value": employee_name},
            {"key": strings.modify_nonattendance_type_text,
                        "value": leave_request.nonattendance_set \
                                                        .all()[0].type.name}
        ]
    })
    context.update(get_common_context(request))
    request.current_app = urls.app_name
    return render(request, "planning/generic_form.html", context)

def can_view_admin_requests_list(user):
    return user.has_perm("planning.validate_request") or \
            user.has_perm("planning.invalidate_request") or \
            user.has_perm("planning.modify_request") or \
            user.has_perm("planning.delete_request") or \
            user.has_perm("planning.print_sheet")

@user_passes_test(can_view_admin_requests_list)
def admin_requests_list(request):
    return HttpResponseRedirect(
        reverse("planning:adminrequestslistbackend") + ADMIN_GET_PARAMS
    )

@user_passes_test(can_view_admin_requests_list)
def admin_requests_list_backend(request):
    show_nonvalidated_message(request)
    headers_title = [
        strings.admin_request_list_header_1,
        strings.admin_request_list_header_2,
        strings.admin_request_list_header_3,
        strings.admin_request_list_header_4,
        strings.admin_request_list_header_5,
    ]
    leave_type = get_leave_nonattendancetype()
    requests = models.LeaveRequest.objects.filter(
                                nonattendance__type=leave_type).distinct()
    requests = filter_requests(requests, request, True)
    if request.method == "POST":
        form = forms.AdminRequestsListActionsForm(request.POST,
                                        user=request.user, queryset=requests)
        if form.is_valid():
            if form.cleaned_data["action"] == "nop":
                messages.add_message(request, messages.WARNING,
                                                            strings.no_action)
                return HttpResponseRedirect(
                                        reverse("planning:adminrequestslist"))
            elif form.cleaned_data["action"] == "modify":
                if not request.user.has_perm("planning.modify_request"):
                    messages.add_message(request, messages.ERROR,
                                                    strings.no_permission)
                    return HttpResponseRedirect(
                                        reverse("planning:adminrequestslist"))
                elif form.cleaned_data["requests"].count() != 1:
                    messages.add_message(request, messages.ERROR,
                                                    strings.only_one_request)
                    return HttpResponseRedirect(
                                        reverse("planning:adminrequestslist"))
                elif form.cleaned_data["requests"][0].validated:
                    messages.add_message(request, messages.WARNING,
                                            strings.already_validated_warning)
                return HttpResponseRedirect(
                        reverse("planning:adminmodifyrequest",
                                args=(form.cleaned_data["requests"][0].pk,)))
            elif form.cleaned_data["action"] == "accept":
                if not request.user.has_perm("planning.validate_request"):
                    messages.add_message(request, messages.ERROR,
                                                    strings.no_permission)
                    return HttpResponseRedirect(
                                        reverse("planning:adminrequestslist"))
                accept_leave_requests(request, form.cleaned_data["requests"])
                return HttpResponseRedirect(
                                        reverse("planning:adminrequestslist"))
            elif form.cleaned_data["action"] == "unaccept":
                 if not request.user.has_perm("planning.invalidate_request"):
                    messages.add_message(request, messages.ERROR,
                                                    strings.no_permission)
                    return HttpResponseRedirect(
                                        reverse("planning:adminrequestslist"))
                 unaccept_leave_requests(request, form.cleaned_data["requests"])
                 return HttpResponseRedirect(
                                        reverse("planning:adminrequestslist"))
            elif form.cleaned_data["action"] == "download_sheet":
                if not request.user.has_perm("planning.print_sheet"):
                    messages.add_message(request, messages.ERROR,
                                                    strings.no_permission)
                    return HttpResponseRedirect(
                                        reverse("planning:adminrequestslist"))
                elif form.cleaned_data["requests"].count() != 1:
                    messages.add_message(request, messages.ERROR,
                                                    strings.only_one_download)
                    return HttpResponseRedirect(
                                        reverse("planning:adminrequestslist"))
                leave_request = form.cleaned_data["requests"][0]
                ods_doc = get_leave_request_sheet(leave_request)
                response = HttpResponse(ods_doc.tobytes(),
                                                content_type=ods_doc.mimetype)
                fr_tz = timezone("Europe/Paris")
                local_date = leave_request.date.astimezone(fr_tz)
                date_str = local_date.isoformat(timespec="seconds")
                filename = str(leave_request.nonattendance_set.all()[0] \
                                .employee.last_name) + "-" + date_str + ".ods"
                response['Content-Disposition'] = \
                                    "attachment; filename=\"" + filename + "\""
                return response
            else: #actions: delete
                if not request.user.has_perm("planning.delete_request"):
                    messages.add_message(request, messages.ERROR,
                                                    strings.no_permission)
                    return HttpResponseRedirect(
                                        reverse("planning:adminrequestslist"))
                delete_leave_requests(request, form.cleaned_data["requests"],
                                                                        True)
                return HttpResponseRedirect(
                                        reverse("planning:adminrequestslist"))
        else:
            if form.errors["requests"][0] == "Ce champ est obligatoire.":
                messages.add_message(request, messages.WARNING,
                                                            strings.no_request)
    else:
        form = forms.AdminRequestsListActionsForm(user=request.user,
                                                            queryset=requests)
    requests_lines = make_requests_lines(form.queryset, form, request, True)
    context = {
        "title": strings.admin_request_list_title,
        "title_header": strings.admin_request_list_title_header,
        "lines": requests_lines,
        "form": form,
        "entry_count": len(form.queryset)
    }
    context.update(get_common_context(request))
    context.update(get_headers_sort_context(headers_title, request))
    context.update(get_filters_context(request, True))
    request.current_app = urls.app_name
    return render(request, "planning/requests_list.html", context)

def can_add_nonattendance(user):
    return user.has_perm("planning.add_non_attendance")

@user_passes_test(can_add_nonattendance)
def add_nonattendance(request):
    show_nonvalidated_message(request)
    context = {}
    PeriodNonattendanceFormSet = formset_factory(forms.PeriodNonattendanceForm,
                                                                        extra=0)
    DayNonattendanceFormSet = formset_factory(forms.DayNonattendanceForm,
                                                                        extra=0)
    DayPartNonattendanceFormSet = formset_factory(
                                        forms.DayPartNonattendanceForm, extra=0)
    if request.method == "POST":
        period_formset = PeriodNonattendanceFormSet(request.POST,
                                                            prefix="p-period")
        day_formset = DayNonattendanceFormSet(request.POST, prefix="p-day")
        daypart_formset = DayPartNonattendanceFormSet(request.POST,
                                                            prefix="p-daypart")
        main_form = forms.AddNonattendanceForm(request.POST)
        if period_formset.is_valid() and day_formset.is_valid() \
                    and daypart_formset.is_valid() and main_form.is_valid():
            if make_nonattendance(main_form, request, period_formset,
                                                day_formset, daypart_formset):
                messages.add_message(request, messages.SUCCESS,
                                            strings.add_nonattendance_success)
                return HttpResponseRedirect(
                                        reverse("planning:addnonattendance"))
        errors_values = []
        for formset in [period_formset, day_formset, daypart_formset]:
            for errors_dict in formset.errors:
                for value in errors_dict.values():
                    errors_values.append(value)
        for value in main_form.errors:
            errors_values.append(value)
        context.update({
            "errors": errors_values
        })
    else:
        period_formset = PeriodNonattendanceFormSet(prefix="p-period")
        day_formset = DayNonattendanceFormSet(prefix="p-day")
        daypart_formset = DayPartNonattendanceFormSet(prefix="p-daypart")
        main_form = forms.AddNonattendanceForm()
    add_metadata(period_formset, strings.period_formset_title,
                                    strings.period_formset_button)
    add_metadata(day_formset, strings.day_formset_title,
                                                    strings.day_formset_button)
    add_metadata(daypart_formset, strings.daypart_formset_title,
                                                strings.daypart_formset_button)
    context.update({
        "form_url": request.path,
        "inline_formsets": [period_formset, day_formset, daypart_formset],
        "submit_text": strings.add_nonattendance_submit,
        "page_name": strings.add_nonattendance_page_name,
        "title": strings.add_nonattendance_title,
        "form": main_form
    })
    context.update(get_common_context(request))
    request.current_app = urls.app_name
    return render(request, "planning/generic_form.html", context)

def can_view_nonattendances_list(user):
    return user.has_perm("planning.modify_non_attendance") or \
                                user.has_perm("planning.delete_non_attendance")

@user_passes_test(can_view_nonattendances_list)
def nonattendances_list(request):
    return HttpResponseRedirect(
        reverse("planning:nonattendanceslistbackend") + ADMIN_GET_PARAMS
    )

@user_passes_test(can_view_nonattendances_list)
def nonattendances_list_backend(request):
    show_nonvalidated_message(request)
    headers_title = [
        strings.nonattendance_list_header_1,
        strings.nonattendance_list_header_2,
        strings.nonattendance_list_header_3,
        strings.nonattendance_list_header_4,
    ]
    leave_type = get_leave_nonattendancetype()
    nonattendances = models.LeaveRequest.objects.exclude(
            nonattendance__isnull=True).exclude(nonattendance__type=leave_type
                                                                    ).distinct()
    nonattendances = filter_requests(nonattendances, request, True, True)
    if request.method == "POST":
        form = forms.NonattendancesListActionsForm(request.POST,
                                    queryset=nonattendances, user=request.user)
        if form.is_valid():
            if form.cleaned_data["action"] == "nop":
                messages.add_message(request, messages.WARNING,
                                                            strings.no_action)
                return HttpResponseRedirect(
                                        reverse("planning:nonattendanceslist"))
            elif form.cleaned_data["action"] == "modify":
                if not request.user.has_perm("planning.modify_non_attendance"):
                    messages.add_message(request, messages.ERROR,
                                                    strings.no_permission)
                    return HttpResponseRedirect(
                                        reverse("planning:nonattendanceslist"))
                elif form.cleaned_data["requests"].count() != 1:
                    messages.add_message(request, messages.ERROR,
                                                    strings.only_one_request)
                    return HttpResponseRedirect(
                                        reverse("planning:adminrequestslist"))
                return HttpResponseRedirect(
                        reverse("planning:adminmodifynonattendance",
                                args=(form.cleaned_data["requests"][0].pk,)))
            else: #actions: delete
                if not request.user.has_perm("planning.delete_non_attendance"):
                    messages.add_message(request, messages.ERROR,
                                                    strings.no_permission)
                    return HttpResponseRedirect(
                                        reverse("planning:nonattendanceslist"))
                delete_leave_requests(request, form.cleaned_data["requests"],
                                                                    True, True)
                return HttpResponseRedirect(
                                        reverse("planning:nonattendanceslist"))
        else:
            if form.errors["requests"][0] == "Ce champ est obligatoire.":
                messages.add_message(request, messages.WARNING,
                                                            strings.no_request)
    else:
        form = forms.NonattendancesListActionsForm(queryset=nonattendances,
                                                            user=request.user)
    requests_lines = make_requests_lines(form.queryset, form, request, True,
                                                                        True)
    context = {
        "title": strings.nonattendance_list_title,
        "title_header": strings.nonattendance_list_title_header,
        "lines": requests_lines,
        "form": form,
        "entry_count": len(form.queryset)
    }
    context.update(get_common_context(request))
    context.update(get_headers_sort_context(headers_title, request))
    context.update(get_filters_context(request, True, True))
    request.current_app = urls.app_name
    return render(request, "planning/requests_list.html", context)

@login_required
def report(request):
    show_nonvalidated_message(request)
    context = get_report_context(request.user)
    context.update({
        "title": strings.report_title,
        "page_name": strings.report_page_name,
    })
    context.update(get_common_context(request))
    request.current_app = urls.app_name
    return render(request, "planning/report.html", context)

def can_view_report(user):
    return user.has_perm("planning.view_statistics")

@user_passes_test(can_view_report)
def admin_report(request):
    show_nonvalidated_message(request)
    current_year = get_current_year()
    context = {
        "title": strings.adminreport_title.format(current_year=current_year),
        "page_name": strings.adminreport_page_name
    }
    reports = []
    current_employees = list(get_user_model().objects.exclude(username="root") \
                    .exclude(leftdate__date__year__lt=current_year).exclude(
                                    joineddate__date__year__gt=current_year) \
                                                        .order_by("last_name"))
    sort_users_by_families(current_employees)
    blank_color = get_blank_color_setting()
    for employee in current_employees:
        report_context = get_report_context(employee)
        report = {}
        report["employee"] = get_display_name(employee)
        report["color"] = get_user_color(employee, blank_color)
        report["values"] = []
        report["values"].append(report_context["acquired_rtt"])
        report["values"].append(report_context["rtt_used"])
        report["values"].append(report_context["rtt_left"])
        report["values"].append(report_context["previous_year_left"])
        report["values"].append(report_context["acquired_leaves"])
        report["values"].append(report_context["fractioned_leaves"])
        report["values"].append(report_context["leaves_used"])
        report["values"].append(report_context["leaves_left"])
        report["values"].append(report_context["illness_count"])
        reports.append(report)
    context["reports"] = reports
    context["categories"] = [
        {
            "name": "RTT",
            "items": [
                {"name": "Acquis {year!s}".format(year=current_year)},
                {"name": "Déjà pris"},
                {"name": "Restant"}
            ]
        },
        {
            "name": "Congés",
            "items": [
                {"name": "Restant de {past_year!s}".format(
                                                past_year=current_year - 1)},
                {"name": "Acquis {year!s}".format(year=current_year)},
                {"name": "dont congés de fractionnement"},
                {"name": "Déjà pris {year!s}".format(year=current_year)},
                {"name": "Restant total"}
            ]
        },
        {
            "name": "Maladie",
            "items": [
                {"name": "Nombre de jours absents"}
            ]
        }
    ]
    context.update(get_common_context(request))
    request.current_app = urls.app_name
    return render(request, "planning/admin_report.html", context)

def can_assign_oncall_week(user):
    return user.has_perm("planning.assign_oncallweek")

@user_passes_test(can_assign_oncall_week)
def select_oncall_week(request):
    show_nonvalidated_message(request)
    context = {}
    fr_tz = timezone("Europe/Paris")
    current_year = django_timezone.now().astimezone(fr_tz).date().year
    if request.method == "POST":
        form = forms.SelectWeekNumberForm(request.POST, year=current_year)
        if form.is_valid():
            raw_week_number = int(form.cleaned_data["week_number"])
            year = current_year + raw_week_number // 54
            week_number = raw_week_number - 53 * (raw_week_number // 54)
            return HttpResponseRedirect(reverse(
                    "planning:setoncallweekemployee", args=(year, week_number)))
        else:
            context.update({
                "errors": form.errors.values
            })
    else:
        form = forms.SelectWeekNumberForm(year=current_year)
    context.update({
        "form_url": request.path,
        "form": form,
        "submit_text": strings.select,
        "page_name": strings.setoncallweekemployee_page_name,
        "title": strings.setoncallweekemployee_title
    })
    context.update(get_common_context(request))
    request.current_app = urls.app_name
    return render(request, "planning/generic_form.html", context)

@user_passes_test(can_assign_oncall_week)
def set_oncall_week_employee(request, year, week_number):
    show_nonvalidated_message(request)
    context = {}
    fr_tz = timezone("Europe/Paris")
    week_selected_info = {"key": "Numéro de semaine:"}
    monday_date = datetime.strptime(str(year) + "-" + str(week_number) + "-1",
                                                        "%Y-%W-%w").date()
    sunday_date = monday_date + timedelta(days=6)
    monday_date_str = django_date(monday_date, "SHORT_DATE_FORMAT")
    sunday_date_str = django_date(sunday_date, "SHORT_DATE_FORMAT")
    week_selected_info["value"] = str(week_number) + " - (" + monday_date_str \
                                            + " - " + sunday_date_str + ")"
    if request.method == "POST":
        form = forms.SetOnCallWeekEmployeeForm(request.POST)
        if form.is_valid():
            if form.cleaned_data["employee"] == None:
                delete_oncall_week(year, week_number)
            else:
                set_oncall_week(form.cleaned_data["employee"], year,
                                                                week_number)
            messages.add_message(request, messages.SUCCESS,
                                                strings.setoncallweek_success)
            return HttpResponseRedirect(reverse("planning:index"))
        else:
            context.update({
                "errors": form.errors.values
            })
    else:
        try:
            oncallweek = models.EmployeeOnCallWeek.objects.get(year=year,
                                                        week_number=week_number)
            form = forms.SetOnCallWeekEmployeeForm(initial=
                                            {"employee": oncallweek.on_call})
        except models.EmployeeOnCallWeek.DoesNotExist:
            form = forms.SetOnCallWeekEmployeeForm()
    context.update({
        "form_url": request.path,
        "form": form,
        "submit_text": strings.save,
        "page_name": strings.setoncallweekemployee_page_name,
        "title": strings.setoncallweekemployee_title,
        "extra_infos": [week_selected_info]
    })
    context.update(get_common_context(request))
    request.current_app = urls.app_name
    return render(request, "planning/generic_form.html", context)

def can_allocate_rtt_tp(user):
    return user.has_perm("planning.allocate_rtt_tp")

@user_passes_test(can_allocate_rtt_tp)
def place_regular_na(request):
    show_nonvalidated_message(request)
    context = {}
    if request.method == "POST":
        form = forms.SelectYearForm(request.POST)
        if form.is_valid():
            place_regular_nonattendance(form.cleaned_data["year"]);
            messages.add_message(request, messages.SUCCESS,
                                            strings.place_regular_na_success)
            return HttpResponseRedirect(reverse("planning:index"))
        else:
            context.update({
                "errors": form.errors.values
            })
    else:
        form = forms.SelectYearForm()
    context.update({
        "form_url": request.path,
        "form": form,
        "submit_text": strings.validate,
        "page_name": strings.place_regular_na_page_name,
        "title": strings.place_regular_na_title,
    })
    context.update(get_common_context(request))
    request.current_app = urls.app_name
    return render(request, "planning/generic_form.html", context)

@user_passes_test(can_allocate_rtt_tp)
def place_employee_regular_na(request):
    show_nonvalidated_message(request)
    context = {}
    if request.method == "POST":
        form = forms.PlaceEmployeeRegularNonattendanceForm(request.POST)
        if form.is_valid():
            place_employee_regular_nonattendances(form.cleaned_data["employee"],
                                    None, form.cleaned_data["placement_start"])
            messages.add_message(request, messages.SUCCESS,
                                            strings.place_regular_na_success)
            return HttpResponseRedirect(reverse("planning:index"))
        else:
            context.update({
                "errors": form.errors.values
            })
    else:
        form = forms.PlaceEmployeeRegularNonattendanceForm()
    context.update({
        "form_url": request.path,
        "form": form,
        "submit_text": strings.validate,
        "page_name": strings.place_regular_na_page_name,
        "title": strings.place_employee_regular_na_title,
    })
    context.update(get_common_context(request))
    request.current_app = urls.app_name
    return render(request, "planning/generic_form.html", context)

def nonvalidated_message_already_shown(request):
    already_shown = False
    storage = messages.get_messages(request)
    for message in storage:
        if message.extra_tags != None and \
                                "nonvalidated_message" in message.extra_tags:
            already_shown = True
            break
    storage.used = False
    return already_shown

def show_nonvalidated_message(request):
    if nonvalidated_message_already_shown(request):
        return
    leave_type = get_leave_nonattendancetype()
    db_request = models.Nonattendance.objects.filter(type=leave_type,
                                                    request__validated=False)
    if request.user.is_authenticated and not (request.user.is_staff or
                            request.user.has_perm("planning.validate_request")):
        db_request = db_request.filter(employee=request.user)
        message_str = strings.employee_request_nonvalidated
    else:
        message_str = strings.some_requests_nonvalidated
    if db_request.count() == 0:
        return
    messages.add_message(request, messages.WARNING, message_str,
                                            extra_tags="nonvalidated_message")

def can_request_for_others(user):
    return user.is_staff or user.subordinate_set.count() > 0

@user_passes_test(can_request_for_others)
def add_leave_request(request):
    show_nonvalidated_message(request)
    context = {}
    PeriodNonattendanceFormSet = formset_factory(forms.PeriodNonattendanceForm,
                                                                        extra=0)
    DayNonattendanceFormSet = formset_factory(forms.DayNonattendanceForm,
                                                                        extra=0)
    DayPartNonattendanceFormSet = formset_factory(
                                        forms.DayPartNonattendanceForm, extra=0)
    if request.method == "POST":
        period_formset = PeriodNonattendanceFormSet(request.POST,
                                                            prefix="p-period")
        day_formset = DayNonattendanceFormSet(request.POST, prefix="p-day")
        daypart_formset = DayPartNonattendanceFormSet(request.POST,
                                                            prefix="p-daypart")
        main_form = forms.AddLeaveRequestForm(request.POST, user=request.user)
        if period_formset.is_valid() and day_formset.is_valid() \
                    and daypart_formset.is_valid() and main_form.is_valid():
            for_user = main_form.cleaned_data["employee"]
            authorized_user = [obj.subordinate for obj in
                                            request.user.subordinate_set.all()]
            if for_user not in authorized_user and not request.user.is_staff:
                messages.add_message(request, messages.ERROR,
                                                        strings.no_permission)
            elif make_leaverequest(main_form, request, period_formset,
                                                day_formset, daypart_formset):
                messages.add_message(request, messages.SUCCESS,
                                            strings.add_leaverequest_success)
                return HttpResponseRedirect(
                                        reverse("planning:addleaverequest"))
        errors_values = []
        for formset in [period_formset, day_formset, daypart_formset]:
            for errors_dict in formset.errors:
                for value in errors_dict.values():
                    errors_values.append(value)
        for value in main_form.errors:
            errors_values.append(value)
        context.update({
            "errors": errors_values
        })
    else:
        period_formset = PeriodNonattendanceFormSet(prefix="p-period")
        day_formset = DayNonattendanceFormSet(prefix="p-day")
        daypart_formset = DayPartNonattendanceFormSet(prefix="p-daypart")
        main_form = forms.AddLeaveRequestForm(user=request.user)
    add_metadata(period_formset, strings.period_formset_title,
                                    strings.period_formset_button)
    add_metadata(day_formset, strings.day_formset_title,
                                                    strings.day_formset_button)
    add_metadata(daypart_formset, strings.daypart_formset_title,
                                                strings.daypart_formset_button)
    context.update({
        "form_url": request.path,
        "inline_formsets": [period_formset, day_formset, daypart_formset],
        "submit_text": strings.add_nonattendance_submit,
        "page_name": strings.add_leaverequest_page_name,
        "title": strings.add_leaverequest_title,
        "form": main_form
    })
    context.update(get_common_context(request))
    request.current_app = urls.app_name
    return render(request, "planning/generic_form.html", context)

def legalnotices(request):
    show_nonvalidated_message(request)
    context = {
        "title": strings.legalnotices_title
    }
    context.update(get_common_context(request))
    request.current_app = urls.app_name
    return render(request, "planning/legalnotices.html", context)
