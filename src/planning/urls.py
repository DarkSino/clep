#/*
# * Copyright © 2018, 2019 Maël A
# *
# * This file is part of CLEP, a leaves scheduler web app.
# *
# * CLEP is free software: you can redistribute it and/or modify
# * it under the terms of the GNU Affero General Public License as
# * published by the Free Software Foundation, either version 3 of the
# * License, or (at your option) any later version.
# *
# * CLEP is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Affero General Public License for more details.
# *
# * You should have received a copy of the GNU Affero General Public License
# * along with CLEP.  If not, see <https://www.gnu.org/licenses/>.
# */
#
#/*
# * This file and CLEP copy code from and use the Django web framework which is
# * distributed under the following license:
# *
# *
# * Copyright (c) Django Software Foundation and individual contributors.
# * All rights reserved.
# *
# * Redistribution and use in source and binary forms, with or without
# * modification, are permitted provided that the following conditions are met:
# *
# *     1. Redistributions of source code must retain the above copyright
# *        notice, this list of conditions and the following disclaimer.
# *
# *     2. Redistributions in binary form must reproduce the above copyright
# *        notice, this list of conditions and the following disclaimer in the
# *        documentation and/or other materials provided with the distribution.
# *
# *     3. Neither the name of Django nor the names of its contributors may be
# *        used to endorse or promote products derived from this software
# *        without specific prior written permission.
# *
# * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# * POSSIBILITY OF SUCH DAMAGE.
# */

from django.urls import path
from django.views.i18n import JavaScriptCatalog

from . import views

app_name = "planning"
urlpatterns = [
    path("", views.index, name="index"),
    path("accounts/login/", views.login, name="login"),
    path("accounts/logout/", views.logout, name="logout"),
    path("accounts/password_change/", views.change_password,
                                                    name="password_change"),
    path("accounts/password_change/done/", views.password_change_done,
                                                name="password_change_done"),
    path("calendar/<int:year>/<int:month>/", views.calendar, name="calendar"),
    path("calendar/history/", views.calendar_history, name="calendar_history"),
    path("leaverequest/", views.leave_request, name="leaverequest"),
    path("jsi18n/", JavaScriptCatalog.as_view(
                            packages=["django.contrib.admin"]), name="jsi18n"),
    path("requestslist/", views.requests_list, name="requestslist"),
    path("requestslistbackend/", views.requests_list_backend,
                                                    name="requestslistbackend"),
    path("modifyrequest/<int:request_id>/",
                                    views.modify_request, name="modifyrequest"),
    path("adminmodifyrequest/<int:request_id>/",
                        views.admin_modify_request, name="adminmodifyrequest"),
    path("adminmodifynonattendance/<int:request_id>/",
            views.admin_modify_nonattendance, name="adminmodifynonattendance"),
    path("adminrequestslist/", views.admin_requests_list,
                                                    name="adminrequestslist"),
    path("adminrequestslistbackend/", views.admin_requests_list_backend,
                                            name="adminrequestslistbackend"),
    path("addnonattendance/", views.add_nonattendance, name="addnonattendance"),
    path("nonattendanceslist/", views.nonattendances_list,
                                                    name="nonattendanceslist"),
    path("nonattendanceslistbackend/", views.nonattendances_list_backend,
                                            name="nonattendanceslistbackend"),
    path("report/", views.report, name="report"),
    path("adminreport/", views.admin_report, name="adminreport"),
    path("setoncallweekemployee/<int:year>/<int:week_number>/",
                views.set_oncall_week_employee, name="setoncallweekemployee"),
    path("selectoncallweek/", views.select_oncall_week,
                                                    name="selectoncallweek"),
    path("placeregularna/", views.place_regular_na, name="placeregularna"),
    path("placeemployeeregularna/", views.place_employee_regular_na,
                                                name="placeemployeeregularna"),
    path("addleaverequest/", views.add_leave_request, name="addleaverequest"),
    path("legalnotices/", views.legalnotices, name="legalnotices")
]
