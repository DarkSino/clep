#/*
# * Copyright © 2018 Maël A
# *
# * This file is part of CLEP, a leaves scheduler web app.
# *
# * CLEP is free software: you can redistribute it and/or modify
# * it under the terms of the GNU Affero General Public License as
# * published by the Free Software Foundation, either version 3 of the
# * License, or (at your option) any later version.
# *
# * CLEP is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Affero General Public License for more details.
# *
# * You should have received a copy of the GNU Affero General Public License
# * along with CLEP.  If not, see <https://www.gnu.org/licenses/>.
# */
#
#/*
# * This file and CLEP copy code from and use the Django web framework which is
# * distributed under the following license:
# *
# *
# * Copyright (c) Django Software Foundation and individual contributors.
# * All rights reserved.
# *
# * Redistribution and use in source and binary forms, with or without
# * modification, are permitted provided that the following conditions are met:
# *
# *     1. Redistributions of source code must retain the above copyright
# *        notice, this list of conditions and the following disclaimer.
# *
# *     2. Redistributions in binary form must reproduce the above copyright
# *        notice, this list of conditions and the following disclaimer in the
# *        documentation and/or other materials provided with the distribution.
# *
# *     3. Neither the name of Django nor the names of its contributors may be
# *        used to endorse or promote products derived from this software
# *        without specific prior written permission.
# *
# * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# * POSSIBILITY OF SUCH DAMAGE.
# */

from django.contrib import admin
from planning import models
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from . import strings
from .forms import SubordinateChoiceField


admin.site.site_header = strings.admin_site_header

class DayPartAdmin(admin.ModelAdmin):
    list_display = ("id", "name")
    list_display_links = list_display
    ordering = ("id",)

class SettingAdmin(admin.ModelAdmin):
    list_display = ("name", "value")
    ordering = ("name",)
    readonly_fields = ("name",)
    fields = list_display
    def has_add_permission(self, request):
        return False
    def has_delete_permission(self, request, obj=None):
        return False

class NonattendanceTypeAdmin(admin.ModelAdmin):
    list_display = ("name", "color", "override_leaves")
    ordering = ("name",)

class HolidayAdmin(admin.ModelAdmin):
    ordering = ("date",)

class VacationAdmin(admin.ModelAdmin):
    list_display = ("name", "start", "end")
    ordering = ("start",)

class FamilyAdmin(admin.ModelAdmin):
    list_display = ("name", "color", "priority")
    ordering = ("name",)

class FunctionAdmin(admin.ModelAdmin):
    ordering = ("name",)

class RTTSettingAdmin(admin.ModelAdmin):
    list_display = ("employee", "week_day", "daypart")

class PartialTimeSettingAdmin(admin.ModelAdmin):
    list_display = ("employee", "week_day")

class JoinedDateInline(admin.StackedInline):
    model = models.JoinedDate
    append_in_fieldset = "Dates importantes"
    template = "planning/admin/leftdate_inline.html"

class LeftDateInline(admin.StackedInline):
    model = models.LeftDate
    append_in_fieldset = "Dates importantes"
    template = "planning/admin/leftdate_inline.html"

class FunctionInline(admin.TabularInline):
    model = models.Function.employees.through
    extra = 0
    append_in_fieldset = "Informations personnelles"
    template = "planning/admin/function_family_inline.html"

class FamilyInline(admin.TabularInline):
    model = models.Family.employees.through
    extra = 0
    append_in_fieldset = "Informations personnelles"
    template = "planning/admin/function_family_inline.html"

class LeavesAcquiredInline(admin.TabularInline):
    model = models.LeavesAcquired
    extra = 0

class SubordinateEmployeeInline(admin.TabularInline):
    model = models.SubordinateEmployee
    fk_name = "employee"
    extra = 0
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "subordinate":
            qs = get_user_model().objects.all().order_by("last_name")
            return SubordinateChoiceField(queryset=qs)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

class UserAdmin(BaseUserAdmin):
    inlines = (JoinedDateInline, LeftDateInline, FunctionInline, FamilyInline,
                            SubordinateEmployeeInline, LeavesAcquiredInline)
    def get_inline_instances(self, request, obj=None):
        return obj and super(UserAdmin, self).get_inline_instances(request,
                                                                    obj) or []

model_registration_list = {
    "Vacation": VacationAdmin,
    "Holiday": HolidayAdmin,
    "NonattendanceType": NonattendanceTypeAdmin,
    "DayPart": DayPartAdmin,
    "Family": FamilyAdmin,
    "Function": FunctionAdmin,
    "OnCallEmployee": None,
    "Setting": SettingAdmin,
    "RTTSetting": RTTSettingAdmin,
    "PartialTimeSetting": PartialTimeSettingAdmin
}

for model_name in model_registration_list:
    model = getattr(models, model_name)
    admin.site.register(model, model_registration_list[model_name])

admin.site.unregister(get_user_model())
admin.site.register(get_user_model(), UserAdmin)
