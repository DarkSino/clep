#/*
# * Copyright © 2018 Maël A
# *
# * This file is part of CLEP, a leaves scheduler web app.
# *
# * CLEP is free software: you can redistribute it and/or modify
# * it under the terms of the GNU Affero General Public License as
# * published by the Free Software Foundation, either version 3 of the
# * License, or (at your option) any later version.
# *
# * CLEP is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Affero General Public License for more details.
# *
# * You should have received a copy of the GNU Affero General Public License
# * along with CLEP.  If not, see <https://www.gnu.org/licenses/>.
# */
#
#/*
# * This file and CLEP copy code from and use the Django web framework which is
# * distributed under the following license:
# *
# *
# * Copyright (c) Django Software Foundation and individual contributors.
# * All rights reserved.
# *
# * Redistribution and use in source and binary forms, with or without
# * modification, are permitted provided that the following conditions are met:
# *
# *     1. Redistributions of source code must retain the above copyright
# *        notice, this list of conditions and the following disclaimer.
# *
# *     2. Redistributions in binary form must reproduce the above copyright
# *        notice, this list of conditions and the following disclaimer in the
# *        documentation and/or other materials provided with the distribution.
# *
# *     3. Neither the name of Django nor the names of its contributors may be
# *        used to endorse or promote products derived from this software
# *        without specific prior written permission.
# *
# * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# * POSSIBILITY OF SUCH DAMAGE.
# */

from django.contrib.auth.forms import AuthenticationForm
from django import forms
from django.contrib.admin.widgets import AdminDateWidget
from django.contrib.auth import get_user_model
from django.template.defaultfilters import date as django_date

from datetime import datetime, timedelta

from . import models
from . import strings

class LeavesAuthenticationForm(AuthenticationForm):
    required_css_class = "required"

class CalendarHistoryForm(forms.Form):
    period = forms.DateField(label=strings.form_calendarhistoryform_period,
                input_formats=["%m/%Y", "%m/%y"],
                    error_messages={"invalid": strings.form_chf_error},
                    help_text="mm/yyyy ou mm/yy")

class PeriodNonattendanceForm(forms.Form):
    period_start = forms.DateField(label=strings.form_periodnaform_start,
                                                        widget=AdminDateWidget)
    period_end = forms.DateField(label=strings.form_periodnaform_end,
                                                        widget=AdminDateWidget)

class DayNonattendanceForm(forms.Form):
    day = forms.DateField(label=strings.form_daynaform_day,
                                                        widget=AdminDateWidget)

class DayPartNonattendanceForm(forms.Form):
    day = forms.DateField(label=strings.form_daypartnaform_day,
                                                        widget=AdminDateWidget)
    daypart = forms.ModelChoiceField(label=strings.form_daypartnaform_daypart,
                                        queryset=models.DayPart.objects.all())

class RequestsListActionsForm(forms.Form):
    action = forms.ChoiceField(label=strings.form_requestslistactionsform_action
        ,choices=[
            ("nop", "---------"),
            ("modify", strings.form_rlaf_modify),
            ("delete", strings.form_rlaf_delete),
            ("download_sheet", strings.form_admin_rlaf_download)
    ])
    requests = forms.ModelMultipleChoiceField(queryset=None,
        widget=forms.CheckboxSelectMultiple(attrs={"class": "action-select"}))
    def __init__(self, *args, user, queryset, **kwargs):
        super(RequestsListActionsForm, self).__init__(*args, **kwargs)
        self.queryset = queryset
        self.fields["requests"].queryset = self.queryset

class AdminRequestsListActionsForm(forms.Form):
    action = forms.ChoiceField(label=strings.form_admin_rlaf_action, choices=[])
    requests = forms.ModelMultipleChoiceField(queryset=None,
        widget=forms.CheckboxSelectMultiple(attrs={"class": "action-select"}))
    def __init__(self, *args, user, queryset, **kwargs):
        super(AdminRequestsListActionsForm, self).__init__(*args, **kwargs)
        self.queryset = queryset
        self.fields["requests"].queryset = self.queryset
        choices_list = [("nop", "---------")]
        if user.has_perm("planning.validate_request"):
            choices_list.append(("accept", strings.form_admin_rlaf_accept))
        if user.has_perm("planning.invalidate_request"):
            choices_list.append(("unaccept", strings.form_admin_rlaf_unaccept))
        if user.has_perm("planning.modify_request"):
            choices_list.append(("modify", strings.form_admin_rlaf_modify))
        if user.has_perm("planning.delete_request"):
            choices_list.append(("delete", strings.form_admin_rlaf_delete))
        if user.has_perm("planning.print_sheet"):
            choices_list.append(
                ("download_sheet", strings.form_admin_rlaf_download)
            )
        self.fields["action"].choices = choices_list

class NonattendancesListActionsForm(forms.Form):
    action = forms.ChoiceField(label=strings.form_nonattendance_laf_action,
                                                                    choices=[])
    requests = forms.ModelMultipleChoiceField(queryset=None,
        widget=forms.CheckboxSelectMultiple(attrs={"class": "action-select"}))
    def __init__(self, *args, user, queryset, **kwargs):
        super().__init__(*args, **kwargs)
        self.queryset = queryset
        self.fields["requests"].queryset = self.queryset
        choices_list = [("nop", "---------")]
        if user.has_perm("planning.modify_non_attendance"):
            choices_list.append(("modify", strings.form_admin_rlaf_modify))
        if user.has_perm("planning.delete_non_attendance"):
            choices_list.append(("delete", strings.form_admin_rlaf_delete))
        self.fields["action"].choices = choices_list

class EmployeeChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, user):
        return user.last_name + " " + user.first_name

class AddNonattendanceForm(forms.Form):
    employee = EmployeeChoiceField(label=strings.form_addna_employee,
                    queryset=get_user_model().objects.order_by("last_name"))
    nonattendance_type = forms.ModelChoiceField(label=strings.form_addna_type,
                            queryset=models.NonattendanceType.objects.all())

class SelectEmployeeForm(forms.Form):
    employee = EmployeeChoiceField(label=strings.form_select_employee_field,
                    queryset=get_user_model().objects.order_by("last_name"))

def add_choices_for_year(year, next_year, choices):
    if next_year:
        year += 1
    for i in range(1, 54):
        monday_date = datetime.strptime(str(year) + "-" + str(i) + "-1",
                                                        "%Y-%W-%w").date()
        if monday_date.year != year:
            break
        sunday_date = monday_date + timedelta(days=6)
        monday_date_str = django_date(monday_date, "SHORT_DATE_FORMAT")
        sunday_date_str = django_date(sunday_date, "SHORT_DATE_FORMAT")
        choice_text = str(i) + " - (" + monday_date_str + " - " + \
                                                        sunday_date_str + ")"
        choices.append((i + (53 if next_year else 0), choice_text))

class SelectWeekNumberForm(forms.Form):
    week_number = forms.ChoiceField(label=strings.weeknumber, choices=[])
    def __init__(self, *args, year, **kwargs):
        super().__init__(*args, **kwargs)
        choices = []
        add_choices_for_year(year, False, choices)
        add_choices_for_year(year, True, choices)
        self.fields["week_number"].choices = choices

class SetOnCallWeekEmployeeForm(forms.Form):
    employee = forms.ModelChoiceField(empty_label="--- Supprimer la semaine",
            label=strings.form_setoncallweekemployee_employee,
                        queryset=models.OnCallEmployee.objects.all(),
                                                                required=False)

class SelectYearForm(forms.Form):
    year = forms.IntegerField(label=strings.form_selectyearform_year)

class PlaceEmployeeRegularNonattendanceForm(forms.Form):
    employee = EmployeeChoiceField(label=
            strings.form_placeregularna_employee,
                    queryset=get_user_model().objects.order_by("last_name"))
    placement_start = forms.DateField(
            label=strings.form_placeregularna_start, widget=AdminDateWidget)

class AddLeaveRequestForm(forms.Form):
    employee = EmployeeChoiceField(label=strings.form_addna_employee,
                                                                queryset=None)
    def __init__(self, *args, user, **kwargs):
        super().__init__(*args, **kwargs)
        qs = get_user_model().objects.all()
        if not user.is_staff:
            qs = qs.filter(supervisor_set__employee=user)
        self.fields["employee"].queryset = qs.order_by("last_name")

class SubordinateChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.first_name + " " + obj.last_name
