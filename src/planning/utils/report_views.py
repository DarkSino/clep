#/*
# * Copyright © 2018, 2019 Maël A
# *
# * This file is part of CLEP, a leaves scheduler web app.
# *
# * CLEP is free software: you can redistribute it and/or modify
# * it under the terms of the GNU Affero General Public License as
# * published by the Free Software Foundation, either version 3 of the
# * License, or (at your option) any later version.
# *
# * CLEP is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Affero General Public License for more details.
# *
# * You should have received a copy of the GNU Affero General Public License
# * along with CLEP.  If not, see <https://www.gnu.org/licenses/>.
# */
#
#/*
# * This file and CLEP copy code from and use the Django web framework which is
# * distributed under the following license:
# *
# *
# * Copyright (c) Django Software Foundation and individual contributors.
# * All rights reserved.
# *
# * Redistribution and use in source and binary forms, with or without
# * modification, are permitted provided that the following conditions are met:
# *
# *     1. Redistributions of source code must retain the above copyright
# *        notice, this list of conditions and the following disclaimer.
# *
# *     2. Redistributions in binary form must reproduce the above copyright
# *        notice, this list of conditions and the following disclaimer in the
# *        documentation and/or other materials provided with the distribution.
# *
# *     3. Neither the name of Django nor the names of its contributors may be
# *        used to endorse or promote products derived from this software
# *        without specific prior written permission.
# *
# * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# * POSSIBILITY OF SUCH DAMAGE.
# */

from .. import models
from ..modules.models_helper import (
    get_leave_nonattendancetype,
    get_daypart_count
)
from ..modules.employee import (
    get_leaves_acquired,
    get_rtt_acquired,
    get_rtt_left,
    get_rtt_used,
    get_illness_count
)
from ..modules.misc import get_current_date
from .common import leaves_left


def get_report_context(user):
    current_date = get_current_date()
    current_year = current_date.year
    last_year_date = current_date.replace(year=current_date.year - 1)
    acquired = get_leaves_acquired(user)
    previous_year_left = leaves_left(user, last_year_date)
    if previous_year_left < 0:
        previous_year_left = 0
    leave_type = get_leave_nonattendancetype()
    leaves_used = models.Nonattendance.objects.filter(employee=user,
                        type=leave_type, date__year=current_year).count()
    leaves_used = leaves_used / get_daypart_count()
    current_week_number = int(current_date.strftime("%W"))
    oncall_employees = []
    for oncall_employee in models.OnCallEmployee.objects.all().order_by(
                                                    "employee__last_name"):
        oncall_employees.append({
            "first_name": oncall_employee.employee.first_name,
            "last_name": oncall_employee.employee.last_name,
            "week_count": oncall_employee.weeks_set.filter(year=current_year,
                                    week_number__lt=current_week_number).count()
        })
    return {
        "current_year": current_year,
        "previous_year": current_year - 1,
        "acquired_leaves": acquired.leaves_count +
                                            acquired.exceptionals_leaves_count,
        "fractioned_leaves": acquired.exceptionals_leaves_count,
        "leaves_left": leaves_left(user, current_date),
        "previous_year_left": previous_year_left,
        "leaves_used": leaves_used,
        "acquired_rtt": get_rtt_acquired(user),
        "rtt_used": get_rtt_used(user),
        "rtt_left": get_rtt_left(user),
        "illness_count": get_illness_count(user),
        "oncall_duty": models.OnCallEmployee.objects.filter(employee=user) \
                                                        .count() != 0,
        "oncall_employees": oncall_employees
    }
