#/*
# * Copyright © 2018, 2019 Maël A
# *
# * This file is part of CLEP, a leaves scheduler web app.
# *
# * CLEP is free software: you can redistribute it and/or modify
# * it under the terms of the GNU Affero General Public License as
# * published by the Free Software Foundation, either version 3 of the
# * License, or (at your option) any later version.
# *
# * CLEP is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Affero General Public License for more details.
# *
# * You should have received a copy of the GNU Affero General Public License
# * along with CLEP.  If not, see <https://www.gnu.org/licenses/>.
# */
#
#/*
# * This file and CLEP copy code from and use the Django web framework which is
# * distributed under the following license:
# *
# *
# * Copyright (c) Django Software Foundation and individual contributors.
# * All rights reserved.
# *
# * Redistribution and use in source and binary forms, with or without
# * modification, are permitted provided that the following conditions are met:
# *
# *     1. Redistributions of source code must retain the above copyright
# *        notice, this list of conditions and the following disclaimer.
# *
# *     2. Redistributions in binary form must reproduce the above copyright
# *        notice, this list of conditions and the following disclaimer in the
# *        documentation and/or other materials provided with the distribution.
# *
# *     3. Neither the name of Django nor the names of its contributors may be
# *        used to endorse or promote products derived from this software
# *        without specific prior written permission.
# *
# * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# * POSSIBILITY OF SUCH DAMAGE.
# */

from planning import models
from planning.modules.models_helper import get_leave_nonattendancetype
from datetime import timedelta
from django.contrib import messages
from .. import strings
from ..modules.models_helper import get_daypart_count

from datetime import date

def leaves_left(employee, date):
    leave_count = 0
    for leaves_acquired in \
                        employee.leavesacquired_set.filter(year__lte=date.year):
        leave_count += leaves_acquired.leaves_count + \
                                    leaves_acquired.exceptionals_leaves_count
    leave_type = get_leave_nonattendancetype()
    nonattendances_count = models.Nonattendance.objects.filter(
                                    employee=employee, type=leave_type).count()
    daypart_count = models.DayPart.objects.count()
    return leave_count - nonattendances_count * (1 / daypart_count)

def leaves_left_ex(employee, date, acquired_year=None):
    if acquired_year == None:
        acquired_year = date.year
    leave_count = 0
    for leaves_acquired in \
                    employee.leavesacquired_set.filter(year__lte=acquired_year):
        leave_count += leaves_acquired.leaves_count + \
                                    leaves_acquired.exceptionals_leaves_count
    leave_type = get_leave_nonattendancetype()
    nonattendances_count = models.Nonattendance.objects.filter(
            employee=employee, type=leave_type, request__date__lt=date).count()
    daypart_count = models.DayPart.objects.count()
    return leave_count - (nonattendances_count / daypart_count)

def add_nonattendance_part(nonattendance_parts, nonattendance):
    nonattendance_parts.append({
        "date": nonattendance.date,
        "daypart": nonattendance.daypart
    })

def handle_empty(current_group, nonattendance_parts, nonattendance):
    if nonattendance.daypart.id == 0:
        current_group.append(nonattendance)
    else:
        add_nonattendance_part(nonattendance_parts, nonattendance)

def handle_group(current_group, nonattendance_lists, daypart_count):
    unwhole_count = len(current_group) % daypart_count
    while unwhole_count > 0:
        add_nonattendance_part(nonattendance_lists["parts"], current_group[-1])
        current_group.pop()
        unwhole_count -= 1
    if len(current_group) != 0:
        if (current_group[-1].date - current_group[0].date).days == 0:
            nonattendance_lists["days"].append(current_group[0].date)
        else:
            nonattendance_lists["periods"].append({
                "start": current_group[0].date,
                "end": current_group[-1].date
            })
        current_group[:] = []

def same_group(current_group, nonattendance, last_daypart_id):
    same = nonattendance.date == current_group[-1].date and \
                nonattendance.daypart.id == current_group[-1].daypart.id + 1
    same = same or (nonattendance.date == current_group[-1].date + \
                timedelta(1) and nonattendance.daypart.id == 0 and \
                            current_group[-1].daypart.id == last_daypart_id)
    same = same or (current_group[-1].date.weekday() == 4 and \
                (nonattendance.date - current_group[-1].date).days == 3 and \
                    nonattendance.daypart.id == 0 and \
                        current_group[-1].daypart.id == last_daypart_id)
    return same

def get_grouped_nonattendances(request):
    nonattendances = request.nonattendance_set.order_by("date", "daypart__id")
    daypart_count = get_daypart_count()
    last_daypart_id = daypart_count - 1
    nonattendance_periods = []
    nonattendance_days = []
    nonattendance_parts = []
    nonattendance_lists = {
        "periods": nonattendance_periods,
        "days": nonattendance_days,
        "parts": nonattendance_parts
    }
    current_group = []
    for i in range(len(nonattendances)):
        nonattendance = nonattendances[i]
        if len(current_group) == 0:
            handle_empty(current_group, nonattendance_parts, nonattendance)
            continue
        elif same_group(current_group, nonattendance, last_daypart_id):
            current_group.append(nonattendance)
            continue
        handle_group(current_group, nonattendance_lists, daypart_count)
        handle_empty(current_group, nonattendance_parts, nonattendance)
    handle_group(current_group, nonattendance_lists, daypart_count)
    return nonattendance_lists

def delete_leave_requests(http_request, requests, admin=False,
                                                        nonattendances=False):
    for request in requests:
        if request.validated and not admin:
            messages.add_message(http_request, messages.ERROR,
                                            strings.delete_already_validated)
            return
    for request in requests:
        request.nonattendance_set.all().delete()
        request.delete()
    if nonattendances:
        messages.add_message(http_request, messages.SUCCESS,
                                                strings.deleted_nonattendances)
    else:
        messages.add_message(http_request, messages.SUCCESS, strings.deleted)

def accept_leave_requests(http_request, requests):
    for request in requests:
        request.validated = True
        request.save()
    messages.add_message(http_request, messages.SUCCESS, strings.accepted)

def unaccept_leave_requests(http_request, requests):
    for request in requests:
        request.validated = False
        request.save()
    messages.add_message(http_request, messages.SUCCESS, strings.unaccepted)

def get_fixed_holidays(year, month):
    month_day_tuples = []
    month_day_tuples.append((1, 1))
    month_day_tuples.append((5, 1))
    month_day_tuples.append((5, 8))
    month_day_tuples.append((7, 14))
    month_day_tuples.append((8, 15))
    month_day_tuples.append((11, 1))
    month_day_tuples.append((11, 11))
    month_day_tuples.append((12, 25))
    fixed_holidays = []
    for month_day in month_day_tuples:
        if month_day[0] != month:
            continue
        fixed_holidays.append(date(year, month, month_day[1]))
    return fixed_holidays

def get_holidays_dates(year, month):
    holidays = models.Holiday.objects.filter(date__year=year, date__month=month)
    holidays = list(holidays)
    for i in range(len(holidays)):
        holidays[i] = holidays[i].date
    holidays += get_fixed_holidays(year, month)
    return holidays

def is_holiday_date(date):
    holidays = get_holidays_dates(date.year, date.month)
    return date in holidays
