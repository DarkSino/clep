#/*
# * Copyright © 2018 Maël A
# *
# * This file is part of CLEP, a leaves scheduler web app.
# *
# * CLEP is free software: you can redistribute it and/or modify
# * it under the terms of the GNU Affero General Public License as
# * published by the Free Software Foundation, either version 3 of the
# * License, or (at your option) any later version.
# *
# * CLEP is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Affero General Public License for more details.
# *
# * You should have received a copy of the GNU Affero General Public License
# * along with CLEP.  If not, see <https://www.gnu.org/licenses/>.
# */
#
#/*
# * This file and CLEP copy code from and use the Django web framework which is
# * distributed under the following license:
# *
# *
# * Copyright (c) Django Software Foundation and individual contributors.
# * All rights reserved.
# *
# * Redistribution and use in source and binary forms, with or without
# * modification, are permitted provided that the following conditions are met:
# *
# *     1. Redistributions of source code must retain the above copyright
# *        notice, this list of conditions and the following disclaimer.
# *
# *     2. Redistributions in binary form must reproduce the above copyright
# *        notice, this list of conditions and the following disclaimer in the
# *        documentation and/or other materials provided with the distribution.
# *
# *     3. Neither the name of Django nor the names of its contributors may be
# *        used to endorse or promote products derived from this software
# *        without specific prior written permission.
# *
# * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# * POSSIBILITY OF SUCH DAMAGE.
# */

from django.db.models import Q
from planning import models

from calendar import monthrange
from copy import deepcopy
from datetime import datetime, timedelta, date

from .common import get_holidays_dates

def get_user_data(user, year, month, alternate_display):
    blank_color = models.Setting.objects.get(
                                        name="couleur_intervalle_vide").value
    day_count = monthrange(year, month)[1]
    daypart_count = models.DayPart.objects.count()
    days_array = []
    for i in range(day_count * daypart_count):
        days_array.append({"color": blank_color, "validated": True,
                                        "length": 1, "index": i, "void": True})
    place_vacations(days_array, year, month, day_count, daypart_count)
    place_weekends(days_array, year, month, daypart_count)
    place_holidays(days_array, year, month)
    nonattendances =  user.nonattendance_set.filter(date__month=month,
                                                                date__year=year)
    place_nonattendances(days_array, nonattendances)
    days_array_set = [days_array, deepcopy(days_array)]
    if hasattr(user, "oncall_duty"):
        oncalls = get_oncalls(user, year, month)
        place_oncalls(days_array_set[0], deepcopy(oncalls), False)
        place_oncalls(days_array_set[1], oncalls, True)
    if alternate_display:
        days_array_set = gather_scatter(days_array_set)
        days_intervals_set = [days_array for days_array in days_array_set]
    else:
        days_array_set = vertically_join_cells(days_array_set)
        days_intervals_set = [days_array for days_array in days_array_set]
    user_data = {
        "first_name": user.first_name,
        "last_name": user.last_name,
        "color": get_user_color(user, blank_color),
        "days_intervals_set": days_intervals_set,
    }
    return user_data

def place_vacations(days_array, year, month, day_count, daypart_count):
    month_start = date(year, month, 1)
    month_end = date(year, month, day_count)
    vacations = models.Vacation.objects.filter(start__lte=month_end,
                                                        end__gte=month_start)
    for vacation in vacations:
        if vacation.start < month_start:
            start_date = month_start
        else:
            start_date = vacation.start
        if vacation.end > month_end:
            end_date = month_end
        else:
            end_date = vacation.end
        start_index = (start_date.day - 1) * daypart_count
        vacation_dayparts =((end_date - start_date).days + 1) * daypart_count
        for i in range(start_index, start_index + vacation_dayparts):
            days_array[i]["color"] = "dummy_vacation"
            days_array[i]["vacation_day"] = True

def place_holidays(days_array, year, month):
    holidays = get_holidays_dates(year, month)
    holiday_color = models.Setting.objects.get(name="couleur_fériés").value
    for holiday in holidays:
        index = (holiday.day - 1) * 2
        days_array[index]["color"] = holiday_color
        days_array[index + 1]["color"] = holiday_color

def place_weekends(days_array, year, month, daypart_count):
    weekend_color = models.Setting.objects.get(name="couleur_weekend").value
    for i in range(len(days_array)):
        day = (days_array[i]["index"] // daypart_count) + 1
        day_date = date(year, month, day)
        if day_date.weekday() == 5 or day_date.weekday() == 6:
            days_array[i]["color"] = weekend_color

def get_user_color(user, default_color):
    color = default_color
    priority = -1000
    for family in user.family_set.all():
        if family.priority > priority:
            color = family.color
            priority = family.priority
    return color

def place_nonattendances(days_array, nonattendances):
    daypart_count = models.DayPart.objects.count()
    for nonattendance in nonattendances:
        index = (nonattendance.date.day - 1) * daypart_count \
                                                    + nonattendance.daypart.id
        days_array[index] = {
            "length": 1,
            "index": index,
            "validated": nonattendance.request.validated,
            "color": nonattendance.type.color
        }

def place_oncalls(days_array, oncalls, overwrite):
    for oncall in oncalls:
        index = oncall["index"]
        if not overwrite and "void" not in days_array[index]:
            continue
        days_array[index] = oncall

def get_oncalls(user, year, month):
    oncalls = []
    daypart_count = models.DayPart.objects.count()
    oncalls_color = models.Setting.objects.get(name="couleur_astreinte").value
    for oncall_week in user.oncall_duty.weeks_set.filter(
                                                Q(year=year) | Q(year=year-1)):
        week_number = oncall_week.week_number
        monday_date = datetime.strptime(str(oncall_week.year) + "-" +
                                    str(week_number) + "-1", "%Y-%W-%w").date()
        sunday_date = monday_date + timedelta(days=6)
        if (monday_date.year != year or monday_date.month != month) and \
                    (sunday_date.year != year or sunday_date.month != month):
            continue
        if monday_date.month == month and monday_date.year == year:
            start_date = monday_date
        else:
            start_date = date(year, month, 1)
        if sunday_date.month == month and sunday_date.year == year:
            end_date = sunday_date
        else:
            end_date = date(year, month, monthrange(year, month)[1])
        start_index = (start_date.day - 1) * daypart_count
        oncall_dayparts = ((end_date - start_date).days + 1) * daypart_count
        for i in range(start_index, start_index + oncall_dayparts):
            oncalls.append({
                "index": i,
                "length": 1,
                "validated": True,
                "color": oncalls_color
            })
    return oncalls

def make_intervals(days_array):
    if len(days_array) == 0:
        return days_array
    start_color = days_array[0]["color"]
    start_fill_height = days_array[0].get("fill_height", False)
    prior_index = days_array[0]["index"]
    interval_length = 0
    i = 1
    while i < len(days_array):
        interval_length += 1
        color = days_array[i]["color"]
        fill_height = days_array[i].get("fill_height", False)
        current_index = days_array[i]["index"]
        if color == start_color and start_fill_height == fill_height and\
                                current_index == prior_index + 1 \
                                                and "void" not in days_array[i]:
            days_array.pop(i)
            prior_index = current_index
            continue
        days_array[i - 1]["length"] = interval_length
        start_color = color
        start_fill_height = fill_height
        prior_index = current_index
        interval_length = 0
        i += 1
    days_array[i - 1]["length"] = interval_length + 1
    return days_array

def vertically_join_cells(days_array_set):
    bottom_offset = 0
    for i in range(len(days_array_set[0])):
        if days_array_set[0][i]["color"] == \
                                days_array_set[1][i + bottom_offset]["color"]:
            days_array_set[0][i]["fill_height"] = True
            days_array_set[1].pop(i + bottom_offset)
            bottom_offset -= 1
    return days_array_set

def gather_scatter(days_array_set):
    daypart_count = models.DayPart.objects.count()
    new_days_array_set = [[] for i in range(daypart_count)]
    oncall_color = models.Setting.objects.get(name="couleur_astreinte").value
    for i in range(len(days_array_set[0]) // daypart_count):
        for j in range(daypart_count):
            new_days_array_set[j].append(
                                    days_array_set[0][i * daypart_count + j])
            if days_array_set[1][i * daypart_count + j]["color"] \
                                                            == oncall_color \
                    and days_array_set[0][i * daypart_count + j]["color"] \
                                                                != oncall_color:
                new_days_array_set[j][-1]["length"] = 1
                new_days_array_set[j].append(
                                    days_array_set[1][i * daypart_count + j])
            else:
                new_days_array_set[j][-1]["length"] = 2
    return new_days_array_set

def sort_users_by_families(users_list):
    def main_family_order(user):
        user_families = list(user.family_set.all())
        if len(user_families) == 0:
            return 200
        main_family = user_families[0]
        for i in range(1, len(user_families)):
            family = user_families[i]
            if family.priority > main_family.priority:
                main_family = family
        return main_family.order
    users_list.sort(key=main_family_order)
