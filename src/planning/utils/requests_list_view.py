#/*
# * Copyright © 2018, 2019 Maël A
# *
# * This file is part of CLEP, a leaves scheduler web app.
# *
# * CLEP is free software: you can redistribute it and/or modify
# * it under the terms of the GNU Affero General Public License as
# * published by the Free Software Foundation, either version 3 of the
# * License, or (at your option) any later version.
# *
# * CLEP is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Affero General Public License for more details.
# *
# * You should have received a copy of the GNU Affero General Public License
# * along with CLEP.  If not, see <https://www.gnu.org/licenses/>.
# */
#
#/*
# * This file and CLEP copy code from and use the Django web framework which is
# * distributed under the following license:
# *
# *
# * Copyright (c) Django Software Foundation and individual contributors.
# * All rights reserved.
# *
# * Redistribution and use in source and binary forms, with or without
# * modification, are permitted provided that the following conditions are met:
# *
# *     1. Redistributions of source code must retain the above copyright
# *        notice, this list of conditions and the following disclaimer.
# *
# *     2. Redistributions in binary form must reproduce the above copyright
# *        notice, this list of conditions and the following disclaimer in the
# *        documentation and/or other materials provided with the distribution.
# *
# *     3. Neither the name of Django nor the names of its contributors may be
# *        used to endorse or promote products derived from this software
# *        without specific prior written permission.
# *
# * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# * POSSIBILITY OF SUCH DAMAGE.
# */

from django.utils.safestring import mark_safe
from django.template.defaultfilters import date as django_date
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.utils import timezone as django_tz
from django.contrib.auth import get_user_model

from pytz import timezone
from datetime import timedelta, date, datetime

import os
import ezodf
from main.settings import BASE_DIR


from planning import models
from planning.modules.models_helper import get_leave_nonattendancetype
from planning.utils.common import get_grouped_nonattendances, leaves_left, \
                                    leaves_left_ex
from planning.utils.calendar_view import get_user_color

from .. import strings

def get_requested_days_str(request):
    nonattendance_lists = get_grouped_nonattendances(request)
    requested_days_str = ""
    first_str_append = True
    for period in nonattendance_lists["periods"]:
        if not first_str_append:
            requested_days_str += ", "
        else:
            first_str_append = False
        requested_days_str += django_date(period["start"]) + " - " \
                                                    + django_date(period["end"])
    for day in nonattendance_lists["days"]:
        if not first_str_append:
            requested_days_str += ", "
        else:
            first_str_append = False
        requested_days_str += django_date(day)
    for part in nonattendance_lists["parts"]:
        if not first_str_append:
            requested_days_str += ", "
        else:
            first_str_append = False
        requested_days_str += django_date(part["date"]) + " " \
                                                        + str(part["daypart"])
    return requested_days_str

def sort_requests(requests, sort_list, admin, nonattendances):
    def requested_days_key(request):
        nonattendance_lists = get_grouped_nonattendances(request)
        min_date = None
        if len(nonattendance_lists["periods"]) != 0:
            min_period_start = nonattendance_lists["periods"][0]["start"]
            for i in range(1, len(nonattendance_lists["periods"])):
                period_start = nonattendance_lists["periods"][i]["start"]
                if period_start < min_period_start:
                    min_period_start = period_start
            if min_date == None or min_period_start < min_date:
                min_date = min_period_start
        if len(nonattendance_lists["days"]) != 0:
            min_day = nonattendance_lists["days"][0]
            for i in range(1, len(nonattendance_lists["days"])):
                day = nonattendance_lists["days"][i]
                if day < min_day:
                    minday = day
            if min_date == None or min_day < min_date:
                min_date = min_day
        if len(nonattendance_lists["parts"]) != 0:
            min_part_date = nonattendance_lists["parts"][0]["date"]
            for i in range(1, len(nonattendance_lists["parts"])):
                part_date = nonattendance_lists["parts"][i]["date"]
                if part_date < min_part_date:
                    min_part_date = part_date
            if min_date == None or min_part_date < min_date:
                min_date = min_part_date
        return min_date
    def request_employee_key(request):
        employee = request.nonattendance_set.all()[0].employee
        return employee.last_name + " " + employee.first_name
    key_funcs = []
    if admin:
        key_funcs.append(request_employee_key)
    key_funcs += [
        lambda request: request.date,
        requested_days_key,
    ]
    if nonattendances:
        key_funcs.append(
                lambda request: request.nonattendance_set.all()[0].type.name)
    else:
        key_funcs.append(lambda request: request.nonattendance_set.count())
        key_funcs.append(lambda request: request.validated)
    requests = list(requests)
    for i in reversed(range(len(sort_list))):
        sort_index = sort_list[i]
        reverse = False
        if sort_index < 0:
            reverse = True
            sort_index = -sort_index
        requests.sort(key=key_funcs[sort_index - 1], reverse=reverse)
    return requests

def make_requests_lines(requests, form, http_request, admin=False, nonattendances=False):
    lines = []
    sort_list = parse_sort_list(http_request.GET.get("o", []))
    daypart_count = models.DayPart.objects.count()
    for request, checkbox in zip(requests, form["requests"]):
        request.form_checkbox = checkbox
    requests = sort_requests(requests, sort_list, admin, nonattendances)
    for request in requests:
        line = []
        line.append(request.form_checkbox.tag)
        if admin:
            employee = request.nonattendance_set.all()[0].employee
            line.append(employee.last_name + " " + employee.first_name)
        line.append(request.date)
        line.append(get_requested_days_str(request))
        if nonattendances:
            line.append(request.nonattendance_set.all()[0].type.name)
        else:
            nonattendance_count = request.nonattendance_set.count()
            day_count = nonattendance_count / daypart_count
            if nonattendance_count % daypart_count == 0:
                day_count = int(day_count)
            line.append(day_count)
        if not nonattendances:
            if request.validated:
                line.append(
                    "<img src=\"" + static("admin/img/icon-yes.svg") + "\" alt=\"Oui\" />")
            else:
                line.append(
                    "<img src=\"" + static("admin/img/icon-no.svg") + "\" alt=\"Oui\" />")
        line[-1] = mark_safe(line[-1])
        lines.append(line)
    return lines

def parse_sort_list(sort_list_str):
    if len(sort_list_str) == 0:
        return []
    else:
        return [int(item) for item in sort_list_str.split(".")]

def compose_sort_list(sort_list):
    return ".".join([str(item) for item in sort_list])

def remove_sort_list_item(sort_list, item):
    if item in sort_list:
        sort_list.remove(item)
    elif -item in sort_list:
        sort_list.remove(-item)

def toggle_sort_list_item(sort_list, item):
    if item in sort_list:
        new_item = -item
    else:
        new_item = item
    remove_sort_list_item(sort_list, item)
    sort_list.insert(0, new_item)

def get_priority(sort_list, item):
    if item in sort_list:
        return sort_list.index(item) + 1
    elif -item in sort_list:
        return sort_list.index(-item) + 1
    else:
        return 0

def get_url_primary(request, sort_list, item):
    base_url = request.build_absolute_uri(request.path)
    sort_list_tmp = sort_list[:]
    toggle_sort_list_item(sort_list_tmp, item)
    url_params = request.GET.copy()
    url_params["o"] = compose_sort_list(sort_list_tmp)
    return base_url + "?" + url_params.urlencode()

def get_url_toggle(request, sort_list, item):
    sort_list_tmp = sort_list[:]
    if item in sort_list_tmp:
        sort_list_tmp[sort_list_tmp.index(item)] = -item
    else:
        sort_list_tmp[sort_list_tmp.index(-item)] = item
    base_url = request.build_absolute_uri(request.path)
    url_params = request.GET.copy()
    url_params["o"] = compose_sort_list(sort_list_tmp)
    return base_url + "?" + url_params.urlencode()

def get_url_remove(request, sort_list, item):
    base_url = request.build_absolute_uri(request.path)
    sort_list_tmp = sort_list[:]
    remove_sort_list_item(sort_list_tmp, item)
    url_params = request.GET.copy()
    url_params["o"] = compose_sort_list(sort_list_tmp)
    return base_url + "?" + url_params.urlencode()

def is_ascending(sort_list, item):
    return not (-item in sort_list)

def get_headers_sort_context(headers_title, request):
    headers = []
    sort_list = parse_sort_list(request.GET.get("o", ""))
    sorted_field_count = len(sort_list)
    for i in range(1, len(headers_title) + 1):
        header_title = headers_title[i - 1]
        header = {"text": header_title, "sortable": True}
        header["url_remove"] = get_url_remove(request, sort_list, i)
        header["url_primary"] = get_url_primary(request, sort_list, i)
        header["ascending"] = is_ascending(sort_list, i)
        header["sort_priority"] = get_priority(sort_list, i)
        if header["sort_priority"] != 0:
            header["url_toggle"] = get_url_toggle(request, sort_list, i)
        headers.append(header)
    return {"headers": headers, "sorted_field_count": sorted_field_count}

def parse_filter_dict(filter_dict_str):
    if len(filter_dict_str) == 0:
        return {}
    filter_dict = {}
    kv_pairs = filter_dict_str.split("-")
    for kv_pair in kv_pairs:
        key, value = kv_pair.split(".")
        filter_dict[key] = value
    return filter_dict

def compose_filter_dict(filter_dict):
    return "-".join([".".join([str(k),str(v)]) for k, v in filter_dict.items()])

def get_choice_query_string(request, filter_dict, filter_id, choice_id):
    key = str(filter_id)
    filter_dict = filter_dict.copy()
    if choice_id == 0:
        if key in filter_dict:
            del filter_dict[key]
    else:
        filter_dict[key] = choice_id
    base_url = request.build_absolute_uri(request.path)
    url_params = request.GET.copy()
    url_params["f"] = compose_filter_dict(filter_dict)
    return base_url + "?" + url_params.urlencode()

def is_selected(filter_dict, filter_id, choice_id):
    key = str(filter_id)
    if key in filter_dict:
        return int(filter_dict[key]) == choice_id
    else:
        return choice_id == 0

def get_filters_context(request, admin=False, nonattendances=False):
    filters = []
    if admin:
        filter_def = {
                "title": strings.filter_employee_title,
                "choices": [{"display": strings.filter_employee_all_choice}]
        }
        for employee in get_user_model().objects.order_by("last_name"):
            employee_name = employee.last_name + " " + employee.first_name
            filter_def["choices"].append({"display": employee_name})
        filters.append(filter_def)
    filters += [
        {
            "title": strings.filter_date_title,
            "choices": (
                {"display": strings.filter_date_all_choice},
                {"display": strings.filter_date_today_choice},
                {"display": strings.filter_date_week_choice},
                {"display": strings.filter_date_month_choice},
                {"display": strings.filter_date_year_choice}
            )
        }
    ]
    if nonattendances:
        filter_def = {
                "title": strings.filter_type_title,
                "choices": [{"display": strings.filter_type_all_choice}]
        }
        leave_name = models.Setting.objects.get(
                                            name="nom_absence_type_conge").value
        types_names = models.NonattendanceType.objects.exclude(name=leave_name)
        for na_type in types_names:
            filter_def["choices"].append({"display": na_type.name})
        filters.append(filter_def)
    else:
        filters += [
            {
                "title": strings.filter_validation_title,
                "choices": (
                    {"display": strings.filter_validation_all_choice},
                    {"display": strings.filter_validation_validated_choice},
                    {"display": strings.filter_validation_unvalidated_choice}
                )
            }
        ]
    filter_dict = parse_filter_dict(request.GET.get("f", ""))
    for i in range(len(filters)):
        filter_ = filters[i]
        for j in range(len(filter_["choices"])):
            choice = filter_["choices"][j]
            choice["selected"] = is_selected(filter_dict, i, j)
            choice["query_string"] = \
                            get_choice_query_string(request, filter_dict, i, j)
    return {"filters": filters}

def filter_requests(requests, http_request, admin=False, nonattendances=False):
    filter_dict = parse_filter_dict(http_request.GET.get("f", ""))
    def date_filter(requests, choice):
        utc_now = django_tz.now()
        now = utc_now.astimezone(timezone("Europe/Paris"))
        choice = int(choice)
        if choice == 0:
            return
        elif choice == 1:
            threshold_date = now.replace(hour=0, minute=0, second=0,
                                                                microsecond=0)
        elif choice == 2:
            utc_now = utc_now - timedelta(days=7)
            now = utc_now.astimezone(timezone("Europe/Paris"))
            threshold_date = now.replace(hour=0, minute=0, second=0,
                                                                microsecond=0)
        elif choice == 3:
            threshold_date = now.replace(day=1, hour=0, minute=0, second=0,
                                                                microsecond=0)
        else:
            threshold_date = now.replace(month=1, day=1, hour=0, minute=0,
                                                    second=0, microsecond=0)

        return requests.filter(date__gte=threshold_date)
    def employee_filter(requests, choice):
        employee_index = int(choice) - 1
        employee= get_user_model().objects.order_by("last_name")[employee_index]
        return requests.filter(nonattendance__employee=employee)
    def nonattendance_type_filter(requests, choice):
        type_index = int(choice) - 1
        leave_name = models.Setting.objects.get(
                                            name="nom_absence_type_conge").value
        type_ = models.NonattendanceType.objects.exclude(
                                                    name=leave_name)[type_index]
        return requests.filter(nonattendance__type=type_)
    filter_funcs = []
    if admin:
        filter_funcs.append(employee_filter)
    filter_funcs.append(date_filter)
    if nonattendances:
        filter_funcs.append(nonattendance_type_filter)
    else:
        filter_funcs.append(lambda requests, choice:
                                    requests.filter(validated=(choice == "1")))
    for filter_id in filter_dict:
        filter_func = filter_funcs[int(filter_id)]
        requests = filter_func(requests, filter_dict[filter_id])
    return requests

def get_leave_request_sheet(request):
    nonattendances = request.nonattendance_set.all()
    current_year = nonattendances[len(nonattendances) - 1].date.year
    ods_doc = ezodf.opendoc(os.path.join(BASE_DIR,
                            "planning/leaverequest_sheet/MODELE_FEUILLE.ods"))
    from ..leaverequest_sheet.settings import fields_pos
    sheet = ods_doc.sheets["MAIN"]
    add_request_date(sheet, request.date, fields_pos)
    employee = nonattendances[0].employee
    add_employee_name(sheet, employee, fields_pos)
    add_employee_functions(sheet, employee, fields_pos)
    place_value(sheet, fields_pos["current_year"], str(current_year))
    place_value(sheet, fields_pos["last_year"], str(current_year - 1))
    add_leaves_count(sheet, request, employee, current_year, fields_pos)
    add_leaves_periods(sheet, request, fields_pos)
    employee_color = get_user_color(employee, "#ffffff")
    from ..leaverequest_sheet.settings import style_name
    set_header_color(ods_doc, employee_color, style_name)
    return ods_doc

def place_value(sheet, field_pos, value):
    if "format" in field_pos:
        places_value = field_pos["format"].format(value)
    else:
        places_value = value
    for place in field_pos.get("places", []):
        sheet[place].set_value(places_value)
    for placement in field_pos.get("placements", []):
        if "format" in placement:
            placement_value = placement["format"].format(value)
        else:
            placement_value = value
        for cell in placement["cells"]:
            sheet[cell].set_value(placement_value)

def add_request_date(sheet, date, fields_pos):
    fr_tz = timezone("Europe/Paris")
    localized_date = date.astimezone(fr_tz)
    request_date = django_date(localized_date, "SHORT_DATETIME_FORMAT")
    place_value(sheet, fields_pos["request_date"], request_date)

def add_employee_name(sheet, employee, fields_pos):
    employee_name = employee.first_name + " " + employee.last_name
    place_value(sheet, fields_pos["employee_name"], employee_name)

def add_employee_functions(sheet, employee, fields_pos):
    functions = employee.function_set.all()
    function_label_s = "s" if len(functions) > 1 else ""
    place_value(sheet, fields_pos["functions_label"], function_label_s)
    if len(functions) >= 1:
        functions_str = functions[0].name
    else:
        functions_str = ""
    for i in range(1, len(functions)):
        functions_str += ", " + functions[i].name
    place_value(sheet, fields_pos["functions"], functions_str)

def add_leaves_count(sheet, request, employee, current_year, fields_pos):
    daypart_count = models.DayPart.objects.all().count()
    leave_type = get_leave_nonattendancetype()
    acquired_set = list(employee.leavesacquired_set.filter(year=current_year))
    if len(acquired_set) != 0:
        leaves_acquired = acquired_set[0]
    else:
        leaves_acquired = type('MockLeavesAcquired', (object,), {
            "leaves_count": 0,
            "exceptionals_leaves_count": 0
        })
    true_previous_year_leaves_left = leaves_left_ex(employee, request.date,
                                                            current_year - 1)
    if true_previous_year_leaves_left < 0:
        previous_year_leaves_left = 0
    else:
        previous_year_leaves_left = true_previous_year_leaves_left
    fr_tz = timezone("Europe/Paris")
    left_last_year_end = leaves_left_ex(employee, fr_tz.localize(
                                datetime(current_year, 1, 1)), current_year - 1)
    total_leaves_acquired = leaves_acquired.leaves_count + \
                                    leaves_acquired.exceptionals_leaves_count
    place_value(sheet, fields_pos["left_last_year"], left_last_year_end)
    place_value(sheet, fields_pos["total_leaves"], total_leaves_acquired)
    leaves_used = models.Nonattendance.objects.filter(employee=employee,
                type=leave_type, date__year=current_year).filter(
                                    request__date__lt=request.date).count()
    place_value(sheet, fields_pos["leaves_used"], leaves_used / daypart_count)
    leaves_left_count = leaves_left_ex(employee, request.date, current_year)
    place_value(sheet, fields_pos["leaves_left"], leaves_left_count)
    day_count_requested = request.nonattendance_set.count() / daypart_count
    place_value(sheet, fields_pos["count_requested"], day_count_requested)
    leaves_left_after = leaves_left_count - day_count_requested
    place_value(sheet, fields_pos["left_after"], leaves_left_after)
    place_value(sheet, fields_pos["previous_year"], previous_year_leaves_left)
    previous_year_after = previous_year_leaves_left - day_count_requested
    if previous_year_after < 0:
        previous_year_after = 0
    place_value(sheet, fields_pos["previous_year_after"], previous_year_after)

def add_leaves_periods(sheet, request, fields_pos):
    nonattendances_lists = get_grouped_nonattendances(request)
    for period in nonattendances_lists["periods"]:
        start_date_str = period["start"].strftime("%d/%m/%Y")
        end_date_str = period["end"].strftime("%d/%m/%Y")
        period_str = "Du " + start_date_str + " au " + end_date_str
        append_str_to_sheet_cells(sheet, fields_pos["periods"]["places"],
                                                                    period_str)
    for day in nonattendances_lists["days"]:
        day_date_str = day.strftime("%d/%m/%Y")
        append_str_to_sheet_cells(sheet, fields_pos["periods"]["places"],
                                                                day_date_str)
    for daypart in nonattendances_lists["parts"]:
        date_str = daypart["date"].strftime("%d/%m/%Y")
        daypart_str = daypart["daypart"].name
        na_daypart_str = date_str + " " + daypart_str
        append_str_to_sheet_cells(sheet, fields_pos["periods"]["places"],
                                                                na_daypart_str)

def append_str_to_sheet_cells(sheet, cells, str_content):
    for cell in cells:
        content = ezodf.text.Paragraph(str_content)
        sheet[cell].append(content)

def set_header_color(ods_doc, html_hex_color_str, style_name):
    style_xmlnode = ods_doc.content.automatic_styles[style_name].xmlnode
    od_style_ns = {"style": "urn:oasis:names:tc:opendocument:xmlns:style:1.0"}
    table_cell_prop = style_xmlnode.find("style:table-cell-properties",
                                                        namespaces=od_style_ns)
    table_cell_prop.set("{urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0}background-color",
                                                            html_hex_color_str)
