#/*
# * Copyright © 2018 Maël A
# *
# * This file is part of CLEP, a leaves scheduler web app.
# *
# * CLEP is free software: you can redistribute it and/or modify
# * it under the terms of the GNU Affero General Public License as
# * published by the Free Software Foundation, either version 3 of the
# * License, or (at your option) any later version.
# *
# * CLEP is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Affero General Public License for more details.
# *
# * You should have received a copy of the GNU Affero General Public License
# * along with CLEP.  If not, see <https://www.gnu.org/licenses/>.
# */
#
#/*
# * This file and CLEP copy code from and use the Django web framework which is
# * distributed under the following license:
# *
# *
# * Copyright (c) Django Software Foundation and individual contributors.
# * All rights reserved.
# *
# * Redistribution and use in source and binary forms, with or without
# * modification, are permitted provided that the following conditions are met:
# *
# *     1. Redistributions of source code must retain the above copyright
# *        notice, this list of conditions and the following disclaimer.
# *
# *     2. Redistributions in binary form must reproduce the above copyright
# *        notice, this list of conditions and the following disclaimer in the
# *        documentation and/or other materials provided with the distribution.
# *
# *     3. Neither the name of Django nor the names of its contributors may be
# *        used to endorse or promote products derived from this software
# *        without specific prior written permission.
# *
# * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# * POSSIBILITY OF SUCH DAMAGE.
# */

from django.contrib import messages
from .. import strings

def convert_to_form_data(initial_data):
    for i in range(len(initial_data["periods"])):
        period_data = initial_data["periods"][i]
        period_data["period_start"] = period_data["start"]
        del period_data["start"]
        period_data["period_end"] = period_data["end"]
        del period_data["end"]
        initial_data["periods"][i] = period_data
    for i in range(len(initial_data["days"])):
        day = initial_data["days"][i]
        day_data = {"day": day}
        initial_data["days"][i] = day_data
    for i in range(len(initial_data["parts"])):
        part_data = initial_data["parts"][i]
        part_data["day"] = part_data["date"]
        del part_data["date"]
        initial_data["parts"][i] = part_data
    return initial_data
