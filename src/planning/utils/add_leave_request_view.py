#/*
# * Copyright © 2018, 2019 Maël A
# *
# * This file is part of CLEP, a leaves scheduler web app.
# *
# * CLEP is free software: you can redistribute it and/or modify
# * it under the terms of the GNU Affero General Public License as
# * published by the Free Software Foundation, either version 3 of the
# * License, or (at your option) any later version.
# *
# * CLEP is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Affero General Public License for more details.
# *
# * You should have received a copy of the GNU Affero General Public License
# * along with CLEP.  If not, see <https://www.gnu.org/licenses/>.
# */
#
#/*
# * This file and CLEP copy code from and use the Django web framework which is
# * distributed under the following license:
# *
# *
# * Copyright (c) Django Software Foundation and individual contributors.
# * All rights reserved.
# *
# * Redistribution and use in source and binary forms, with or without
# * modification, are permitted provided that the following conditions are met:
# *
# *     1. Redistributions of source code must retain the above copyright
# *        notice, this list of conditions and the following disclaimer.
# *
# *     2. Redistributions in binary form must reproduce the above copyright
# *        notice, this list of conditions and the following disclaimer in the
# *        documentation and/or other materials provided with the distribution.
# *
# *     3. Neither the name of Django nor the names of its contributors may be
# *        used to endorse or promote products derived from this software
# *        without specific prior written permission.
# *
# * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# * POSSIBILITY OF SUCH DAMAGE.
# */

from .. import strings
from .. import models
from ..modules.models_helper import get_leave_nonattendancetype

from .leave_request_view import (
    add_daypart_forms_dayparts,
    add_day_forms_dayparts,
    add_period_forms_dayparts,
    add_errors_to_forms,
    employee_already_nonattendant,
    filter_past_dayparts,
    remove_weekends_and_holidays
)

from django.utils import timezone
from django.contrib import messages
from pytz import timezone as pytz_tz
from .common import leaves_left


def make_leaverequest(main_form, request, period_formset, day_formset,
                                                            daypart_formset):
    user = main_form.cleaned_data["employee"]
    dayparts_requested = []
    add_daypart_forms_dayparts(daypart_formset, dayparts_requested)
    add_day_forms_dayparts(day_formset, dayparts_requested)
    add_period_forms_dayparts(period_formset, dayparts_requested)
    past_dayparts_requested = filter_past_dayparts(dayparts_requested)
    if past_dayparts_requested != []:
        add_errors_to_forms(past_dayparts_requested, strings.request_in_past)
        return False
    colliding_dayparts = employee_already_nonattendant(user, dayparts_requested)
    if colliding_dayparts != []:
        add_errors_to_forms(colliding_dayparts,
                                        strings.employee_already_nonattendant)
        return False
    remove_weekends_and_holidays(dayparts_requested)
    if dayparts_requested == []:
        messages.add_message(request, messages.ERROR, strings.request_empty_2)
        return False
    daypart_count = models.DayPart.objects.count()
    fr_tz = pytz_tz("Europe/Paris")
    available_leaves = leaves_left(user,timezone.now().astimezone(fr_tz).date())
    if len(dayparts_requested) / daypart_count > available_leaves:
        message = strings.no_leaves_left_employee.format(
            asked_count=len(dayparts_requested) / daypart_count,
            available_count=available_leaves,
            asked_suffix= "s" if len(dayparts_requested) > 1 else "",
            available_suffix = "s" if available_leaves > 1 else ""
        )
        messages.add_message(request, messages.ERROR, message)
        return False
    leave_type = get_leave_nonattendancetype()
    request = models.LeaveRequest()
    request.save()
    for daypart in dayparts_requested:
        nonattendance = models.Nonattendance(request=request, employee=user,
                            date=daypart["date"], daypart=daypart["daypart"],
                                                        type=leave_type)
        nonattendance.save()
    return True
