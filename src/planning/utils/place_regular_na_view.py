#/*
# * Copyright © 2018 Maël A
# *
# * This file is part of CLEP, a leaves scheduler web app.
# *
# * CLEP is free software: you can redistribute it and/or modify
# * it under the terms of the GNU Affero General Public License as
# * published by the Free Software Foundation, either version 3 of the
# * License, or (at your option) any later version.
# *
# * CLEP is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Affero General Public License for more details.
# *
# * You should have received a copy of the GNU Affero General Public License
# * along with CLEP.  If not, see <https://www.gnu.org/licenses/>.
# */
#
#/*
# * This file and CLEP copy code from and use the Django web framework which is
# * distributed under the following license:
# *
# *
# * Copyright (c) Django Software Foundation and individual contributors.
# * All rights reserved.
# *
# * Redistribution and use in source and binary forms, with or without
# * modification, are permitted provided that the following conditions are met:
# *
# *     1. Redistributions of source code must retain the above copyright
# *        notice, this list of conditions and the following disclaimer.
# *
# *     2. Redistributions in binary form must reproduce the above copyright
# *        notice, this list of conditions and the following disclaimer in the
# *        documentation and/or other materials provided with the distribution.
# *
# *     3. Neither the name of Django nor the names of its contributors may be
# *        used to endorse or promote products derived from this software
# *        without specific prior written permission.
# *
# * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# * POSSIBILITY OF SUCH DAMAGE.
# */

from django.contrib.auth import get_user_model
from django.db import IntegrityError
from datetime import datetime, timedelta

from .. import models

def place_regular_nonattendance(year):
    User = get_user_model()
    for user in User.objects.all():
        place_employee_regular_nonattendances(user, year)

def place_employee_regular_nonattendances(employee, year, start_date=None):
    try:
        rtt_setting = models.RTTSetting.objects.get(employee=employee)
        place_user_regular_nonattendance(employee, year, rtt_setting.week_day,
                                rtt_setting.daypart, False, start_date)
    except models.RTTSetting.DoesNotExist:
        pass
    try:
        partialtime_setting = models.PartialTimeSetting.objects.get(
                                                            employee=employee)
        place_user_regular_nonattendance(employee, year,
            partialtime_setting.week_day, models.ENTIRE_DAY, True, start_date)
    except models.PartialTimeSetting.DoesNotExist:
        pass

def place_user_regular_nonattendance(user, year, week_day, daypart,
                                                partial_time, start_date=None):
    if start_date == None:
        start_date = datetime.strptime(str(year) + "-1-" + str(week_day),
                                                            "%Y-%W-%w").date()
    else:
        while start_date.isoweekday() % 7 != week_day:
            start_date += timedelta(days=1)
        year = start_date.year
    if daypart == models.ENTIRE_DAY and not partial_time:
        period = timedelta(days=14)
    else:
        period = timedelta(days=7)
    if partial_time:
        type_name="temps partiel"
    else:
        type_name="RTT"
    na_type = models.NonattendanceType.objects.get(name=type_name)
    morning = models.DayPart.objects.get(name="matin")
    afternoon = models.DayPart.objects.get(name="après-midi")
    try:
        acquired = models.LeavesAcquired.objects.get(employee=user, year=year)
        rtt_count = acquired.rtt_count
    except models.LeavesAcquired.DoesNotExist:
        rtt_count = 0
    rtt_used = 0
    while ((not partial_time and rtt_count - rtt_used > 0) \
                                or partial_time) and start_date.year == year:
        if is_vacation_date(start_date) and not partial_time:
            start_date += period
            continue
        request = models.LeaveRequest(validated=True)
        request.save()
        if daypart == models.MORNING or daypart == models.ENTIRE_DAY:
            nonattendance = models.Nonattendance(request=request, employee=user,
                    date=start_date, daypart=morning, type=na_type)
            try:
                nonattendance.save()
            except IntegrityError:
                pass
            rtt_used += 0.5
        if daypart == models.AFTERNOON or daypart == models.ENTIRE_DAY:
            nonattendance = models.Nonattendance(request=request, employee=user,
                    date=start_date, daypart=afternoon, type=na_type)
            try:
                nonattendance.save()
            except IntegrityError:
                pass
            rtt_used += 0.5
        start_date += period

def is_vacation_date(date, cache=[]):
    if len(cache) == 0:
        vacations = models.Vacation.objects.filter(end__gte=date)
        cache[:] = list(vacations)
    else:
        vacations = cache
    for vacation in vacations:
        if vacation.start < date < vacation.end:
            return True
    return False
