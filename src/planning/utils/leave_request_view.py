#/*
# * Copyright © 2018, 2019 Maël A
# *
# * This file is part of CLEP, a leaves scheduler web app.
# *
# * CLEP is free software: you can redistribute it and/or modify
# * it under the terms of the GNU Affero General Public License as
# * published by the Free Software Foundation, either version 3 of the
# * License, or (at your option) any later version.
# *
# * CLEP is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Affero General Public License for more details.
# *
# * You should have received a copy of the GNU Affero General Public License
# * along with CLEP.  If not, see <https://www.gnu.org/licenses/>.
# */
#
#/*
# * This file and CLEP copy code from and use the Django web framework which is
# * distributed under the following license:
# *
# *
# * Copyright (c) Django Software Foundation and individual contributors.
# * All rights reserved.
# *
# * Redistribution and use in source and binary forms, with or without
# * modification, are permitted provided that the following conditions are met:
# *
# *     1. Redistributions of source code must retain the above copyright
# *        notice, this list of conditions and the following disclaimer.
# *
# *     2. Redistributions in binary form must reproduce the above copyright
# *        notice, this list of conditions and the following disclaimer in the
# *        documentation and/or other materials provided with the distribution.
# *
# *     3. Neither the name of Django nor the names of its contributors may be
# *        used to endorse or promote products derived from this software
# *        without specific prior written permission.
# *
# * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# * POSSIBILITY OF SUCH DAMAGE.
# */

import json
from django.utils.translation import gettext
from django.template.defaultfilters import capfirst
from django.forms import ValidationError
from django.utils import timezone
from django.contrib import messages

from datetime import timedelta
from pytz import timezone as pytz_tz

from .. import models
from ..modules.models_helper import get_leave_nonattendancetype
from .. import strings
from .common import leaves_left, is_holiday_date

def add_metadata(formset, field_title, add_text):
    formset.verbose_name_plural = field_title
    formset.inline_formset_data = json.dumps({
        'name': '#%s' % formset.prefix,
        'options': {
            'prefix': formset.prefix,
            'addText': add_text,
            'deleteText': gettext('Remove'),
        }
    })
    formset.forms = formset.forms + [formset.empty_form]

def add_to_dayparts_list(dayparts_list, new_elem):
    already_present = False
    for item in dayparts_list:
        if item["date"] == new_elem["date"] and \
                                        item["daypart"] == new_elem["daypart"]:
            already_present = True
    if not already_present:
        dayparts_list.append(new_elem)

def add_all_dayparts(dayparts_list, day, form):
    for daypart in models.DayPart.objects.all():
        add_to_dayparts_list(dayparts_list, {
            "date": day,
            "daypart": daypart,
            "form": form
        })

def employee_already_nonattendant(user, dayparts, modified_request=None, override=True):
    colliding_dayparts = []
    i = 0
    while i < len(dayparts):
        daypart = dayparts[i]
        try:
            nonattendance = models.Nonattendance.objects.get(employee=user,
                            date=daypart["date"], daypart=daypart["daypart"])
            if nonattendance.request != modified_request:
                if nonattendance.type.override_leaves and override:
                    dayparts.pop(i)
                    continue
                else:
                    daypart["nonattendance"] = nonattendance
                    colliding_dayparts.append(daypart)
        except models.Nonattendance.DoesNotExist:
            pass
        i += 1
    return colliding_dayparts

def filter_past_dayparts(dayparts):
    past_dayparts = []
    days_listed = set()
    today = timezone.now().date()
    for daypart in dayparts:
        if daypart["date"] <= today and daypart["date"] not in days_listed:
            past_dayparts.append(daypart)
            days_listed.add(daypart["date"])
    return past_dayparts

def add_errors_to_forms(error_dayparts, error_string):
    for daypart in error_dayparts:
        form = daypart["form"]
        params = {
            "date": daypart["date"].strftime("%d/%m/%Y"),
            "daypart": daypart["daypart"]
        }
        if "nonattendance" in daypart:
            params.update({"type": daypart["nonattendance"].type })
        form.add_error(None, ValidationError(error_string, params=params))

def remove_weekends_and_holidays(dayparts):
    i = 0
    while i < len(dayparts):
        daypart = dayparts[i]
        if is_holiday_date(daypart["date"]):
            dayparts.pop(i)
            continue
        if daypart["date"].weekday() == 5 or daypart["date"].weekday() == 6:
            dayparts.pop(i)
            continue
        i += 1

def add_daypart_forms_dayparts(daypart_formset, dayparts_requested):
    for form in daypart_formset:
        if form.cleaned_data != {} and \
                                    not form.cleaned_data.get("DELETE", False):
            add_to_dayparts_list(dayparts_requested, {
                "date": form.cleaned_data["day"],
                "daypart": form.cleaned_data["daypart"],
                "form": form
            })

def add_day_forms_dayparts(day_formset, dayparts_requested):
    for form in day_formset:
        if form.cleaned_data != {} and \
                                    not form.cleaned_data.get("DELETE", False):
            add_all_dayparts(dayparts_requested, form.cleaned_data["day"], form)

def add_period_forms_dayparts(period_formset, dayparts_requested):
    for form in period_formset:
        if form.cleaned_data != {} and \
                                    not form.cleaned_data.get("DELETE", False):
            start_date = form.cleaned_data["period_start"]
            day_count = (form.cleaned_data["period_end"] - start_date).days + 1
            for i in range(day_count):
                offset = timedelta(days=1) * i
                date = start_date + offset
                add_all_dayparts(dayparts_requested, date, form)

def make_leave_request(user, request, period_formset, day_formset,
                                                            daypart_formset):
    dayparts_requested = []
    add_daypart_forms_dayparts(daypart_formset, dayparts_requested)
    add_day_forms_dayparts(day_formset, dayparts_requested)
    add_period_forms_dayparts(period_formset, dayparts_requested)
    past_dayparts_requested = filter_past_dayparts(dayparts_requested)
    if past_dayparts_requested != []:
        add_errors_to_forms(past_dayparts_requested, strings.request_in_past)
        return False
    colliding_dayparts = employee_already_nonattendant(user, dayparts_requested)
    if colliding_dayparts != []:
        add_errors_to_forms(colliding_dayparts, strings.already_nonattendant)
        return False
    remove_weekends_and_holidays(dayparts_requested)
    if dayparts_requested == []:
        messages.add_message(request, messages.ERROR, strings.request_empty)
        return False
    daypart_count = models.DayPart.objects.count()
    fr_tz = pytz_tz("Europe/Paris")
    available_leaves = leaves_left(user,timezone.now().astimezone(fr_tz).date())
    if len(dayparts_requested) / daypart_count > available_leaves:
        message = strings.no_leaves_left.format(
            asked_count=len(dayparts_requested) / daypart_count,
            available_count=available_leaves,
            asked_suffix= "s" if len(dayparts_requested) > 1 else "",
            available_suffix = "s" if available_leaves > 1 else ""
        )
        messages.add_message(request, messages.ERROR, message)
        return False
    leave_type = get_leave_nonattendancetype()
    request = models.LeaveRequest()
    request.save()
    for daypart in dayparts_requested:
        nonattendance = models.Nonattendance(request=request, employee=user,
                            date=daypart["date"], daypart=daypart["daypart"],
                                                        type=leave_type)
        nonattendance.save()
    return True

def modify_leave_request(user, request, leave_request, period_formset,
                                    day_formset, daypart_formset, admin=False):
    dayparts_requested = []
    add_daypart_forms_dayparts(daypart_formset, dayparts_requested)
    add_day_forms_dayparts(day_formset, dayparts_requested)
    add_period_forms_dayparts(period_formset, dayparts_requested)
    past_dayparts_requested = filter_past_dayparts(dayparts_requested)
    if not admin and past_dayparts_requested != []:
        add_errors_to_forms(past_dayparts_requested, strings.request_in_past)
        return False
    colliding_dayparts = employee_already_nonattendant(user, dayparts_requested,
                                                            leave_request)
    if colliding_dayparts != []:
        add_errors_to_forms(colliding_dayparts, strings.already_nonattendant)
        return False
    remove_weekends_and_holidays(dayparts_requested)
    if dayparts_requested == []:
        messages.add_message(request, messages.ERROR, strings.request_empty)
        return False
    daypart_count = models.DayPart.objects.count()
    fr_tz = pytz_tz("Europe/Paris")
    available_leaves = leaves_left(user,timezone.now().astimezone(fr_tz).date())
    already_counted = leave_request.nonattendance_set.count()
    if (len(dayparts_requested) - already_counted) / daypart_count \
                                                            > available_leaves:
        message = strings.no_leaves_left.format(
            asked_count=len(dayparts_requested) / daypart_count,
            available_count=available_leaves + already_counted / daypart_count,
            asked_suffix= "s" if len(dayparts_requested) > 1 else "",
            available_suffix = "s" if available_leaves > 1 else ""
        )
        messages.add_message(request, messages.ERROR, message)
        return False
    leave_type = get_leave_nonattendancetype()
    request = leave_request
    request.nonattendance_set.all().delete()
    if not admin:
        request.date = timezone.now()
        request.save()
    for daypart in dayparts_requested:
        nonattendance = models.Nonattendance(request=request, employee=user,
                            date=daypart["date"], daypart=daypart["daypart"],
                                                        type=leave_type)
        nonattendance.save()
    return True
