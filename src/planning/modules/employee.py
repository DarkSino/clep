#/*
# * Copyright © 2019 Maël A
# *
# * This file is part of CLEP, a leaves scheduler web app.
# *
# * CLEP is free software: you can redistribute it and/or modify
# * it under the terms of the GNU Affero General Public License as
# * published by the Free Software Foundation, either version 3 of the
# * License, or (at your option) any later version.
# *
# * CLEP is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Affero General Public License for more details.
# *
# * You should have received a copy of the GNU Affero General Public License
# * along with CLEP.  If not, see <https://www.gnu.org/licenses/>.
# */
#
#/*
# * This file and CLEP copy code from and use the Django web framework which is
# * distributed under the following license:
# *
# *
# * Copyright (c) Django Software Foundation and individual contributors.
# * All rights reserved.
# *
# * Redistribution and use in source and binary forms, with or without
# * modification, are permitted provided that the following conditions are met:
# *
# *     1. Redistributions of source code must retain the above copyright
# *        notice, this list of conditions and the following disclaimer.
# *
# *     2. Redistributions in binary form must reproduce the above copyright
# *        notice, this list of conditions and the following disclaimer in the
# *        documentation and/or other materials provided with the distribution.
# *
# *     3. Neither the name of Django nor the names of its contributors may be
# *        used to endorse or promote products derived from this software
# *        without specific prior written permission.
# *
# * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# * POSSIBILITY OF SUCH DAMAGE.
# */

from .misc import get_current_year, get_current_date
from .. import models
from .models_helper import (
    get_rtt_nonattendancetype,
    get_illness_nonattendancetype,
    get_daypart_count
)


def get_leaves_acquired(employee, year=None):
    if year == None:
        year = get_current_year()
    wrapped_acquired = list(models.LeavesAcquired.objects.filter(
                                                employee=employee, year=year))
    if len(wrapped_acquired) == 1:
        return wrapped_acquired[0]
    else:
        return type('MockLeavesAcquired', (object,), {
            "leaves_count": 0,
            "exceptionals_leaves_count": 0,
            "rtt_count": 0
        })

def get_rtt_used(employee, year=None):
    if year == None:
        year = get_current_year()
    rtt_type = get_rtt_nonattendancetype()
    rtt_count = models.Nonattendance.objects.filter(employee=employee,
                type=rtt_type, date__year=year).count()
    return rtt_count / get_daypart_count()

def get_rtt_acquired(employee, year=None):
    leaves_acquired = get_leaves_acquired(employee, year)
    return leaves_acquired.rtt_count

def get_rtt_left(employee, year=None):
    return get_rtt_acquired(employee, year) - get_rtt_used(employee, year)

def get_illness_count(employee, date=None):
    if date == None:
        date = get_current_date()
    illness_type = get_illness_nonattendancetype()
    illness_count = models.Nonattendance.objects.filter(employee=employee,
                type=illness_type, date__year=date.year, date__lte=date).count()
    return illness_count / get_daypart_count()

def get_display_name(employee):
    return employee.last_name + " " + employee.first_name
