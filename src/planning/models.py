#/*
# * Copyright © 2018, 2019 Maël A
# *
# * This file is part of CLEP, a leaves scheduler web app.
# *
# * CLEP is free software: you can redistribute it and/or modify
# * it under the terms of the GNU Affero General Public License as
# * published by the Free Software Foundation, either version 3 of the
# * License, or (at your option) any later version.
# *
# * CLEP is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Affero General Public License for more details.
# *
# * You should have received a copy of the GNU Affero General Public License
# * along with CLEP.  If not, see <https://www.gnu.org/licenses/>.
# */
#
#/*
# * This file and CLEP copy code from and use the Django web framework which is
# * distributed under the following license:
# *
# *
# * Copyright (c) Django Software Foundation and individual contributors.
# * All rights reserved.
# *
# * Redistribution and use in source and binary forms, with or without
# * modification, are permitted provided that the following conditions are met:
# *
# *     1. Redistributions of source code must retain the above copyright
# *        notice, this list of conditions and the following disclaimer.
# *
# *     2. Redistributions in binary form must reproduce the above copyright
# *        notice, this list of conditions and the following disclaimer in the
# *        documentation and/or other materials provided with the distribution.
# *
# *     3. Neither the name of Django nor the names of its contributors may be
# *        used to endorse or promote products derived from this software
# *        without specific prior written permission.
# *
# * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# * POSSIBILITY OF SUCH DAMAGE.
# */

from django.db import models
from django.core.validators import MaxValueValidator
from django.conf import settings

from . import strings


class Function(models.Model):
    class Meta:
        verbose_name = strings.model_function_name
    name = models.CharField(strings.model_function_name_field, max_length=100,
                                                                    unique=True)
    employees = models.ManyToManyField(
            settings.AUTH_USER_MODEL,
            through="FunctionEmployeeRelation",
            verbose_name=strings.model_function_employees_name
    )
    def __str__(self):
        return self.name

class FunctionEmployeeRelation(models.Model):
    class Meta:
        verbose_name=strings.model_functionemployeerelation_name
    function_id = models.ForeignKey(
        Function,
        models.PROTECT,
        verbose_name=strings.model_fuer_fid_name
    )
    user_id = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        models.CASCADE,
        verbose_name=strings.model_fuer_uid_name
    )
    def __str__(self):
        return ""

class Family(models.Model):
    class Meta:
        verbose_name = strings.model_family_name
    name = models.CharField(strings.model_family_name_name, max_length=100,
                                                                    unique=True)
    color = models.CharField(strings.model_family_color_name, max_length=255)
    priority = models.SmallIntegerField(strings.model_family_priority_name,
                                                                    unique=True)
    employees = models.ManyToManyField(
            settings.AUTH_USER_MODEL,
            through="FamilyEmployeeRelation",
            verbose_name=strings.model_family_employees_name
    )
    order = models.PositiveSmallIntegerField(strings.model_family_order)
    def __str__(self):
        return self.name

class FamilyEmployeeRelation(models.Model):
    class Meta:
        verbose_name=strings.model_familyemployeerelation_name
    family_id = models.ForeignKey(
        Family,
        models.PROTECT,
        verbose_name=strings.model_faer_fid
    )
    user_id = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        models.CASCADE,
        verbose_name=strings.model_faer_uid
    )
    def __str__(self):
        return ""

#Liste des employées pouvant être d'astreinte
class OnCallEmployee(models.Model):
    class Meta:
        verbose_name = strings.model_oncallemployee_name
        verbose_name_plural = strings.model_oncallemployee_name_plural
    employee = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        models.PROTECT,
        related_name="oncall_duty",
        verbose_name=strings.model_oncallemployee_employee_name
    )
    def __str__(self):
        return self.employee.last_name + " " + self.employee.first_name

class EmployeeOnCallWeek(models.Model):
    class Meta:
        verbose_name = strings.model_employeeoncallweek_name
        unique_together = (("year", "week_number"),)
        permissions = (
            ("assign_oncallweek", "Peut attribuer les astreintes"),
        )
    on_call = models.ForeignKey(
        OnCallEmployee,
        models.PROTECT,
        related_name="weeks_set",
        verbose_name=strings.model_eocw_oncall_name
    )
    year = models.PositiveSmallIntegerField(strings.model_eocw_year)
    week_number = models.PositiveSmallIntegerField(
        strings.model_eocw_week_number,
        validators = [
            MaxValueValidator(53)
        ]
    )

class DayPart(models.Model):
    class Meta:
        verbose_name = strings.model_daypart_name
        verbose_name_plural = strings.model_daypart_name_plural
    id = models.PositiveSmallIntegerField(strings.model_daypart_id,
                                                            primary_key=True)
    name = models.CharField(strings.model_daypart_name, max_length=10,
                                                                    unique=True)
    def __str__(self):
        return self.name

class NonattendanceType(models.Model):
    class Meta:
        verbose_name = strings.model_nonattendancetype_name
        verbose_name_plural = strings.model_nonattendancetype_name_plural
    name = models.CharField(strings.model_nonattendancetype_name_field,
                                                    max_length=50, unique=True)
    color = models.CharField(strings.model_nonattendancetype_color,
                                                                max_length=7)
    override_leaves = models.BooleanField(
                                strings.model_nonattendancetype_override_leaves)
    def __str__(self):
        return self.name

class LeaveRequest(models.Model):
    class Meta:
        verbose_name = strings.model_leaverequest_name
        verbose_name_plural = strings.model_leaverequest_name_plural
        permissions = (
            ("validate_request", "Peut valider les demandes de congés"),
            ("invalidate_request", "Peut invalider les demandes de congés"),
            ("modify_request", "Peut modifier les demandes de congés"),
            ("delete_request", "Peut supprimer les demandes de congés"),
            ("print_sheet", "Peut imprimer les feuilles de congés")
        )
    date = models.DateTimeField(auto_now_add=True, editable=False, blank=True)
    validated = models.BooleanField(strings.model_leaverequest_validated,
                                                default=False, editable=False)

class Nonattendance(models.Model):
    class Meta:
        unique_together = ("employee", "date", "daypart")
        verbose_name = strings.model_nonattendance_name
        permissions = (
            ("add_non_attendance", "Peut ajouter une absence"),
            ("modify_non_attendance", "Peut modifier une absence"),
            ("delete_non_attendance", "Peut supprimer une absence")
        )
    request = models.ForeignKey(
        LeaveRequest,
        models.PROTECT,
        verbose_name=strings.model_nonattendance_request
    )
    employee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        models.PROTECT,
        verbose_name=strings.model_nonattendance_employee
    )
    date = models.DateField()
    daypart = models.ForeignKey(
        DayPart,
        models.PROTECT,
        verbose_name=strings.model_nonattendance_daypart,
        related_name="+"
    )
    type = models.ForeignKey(
        NonattendanceType,
        models.PROTECT
    )
    def __str__(self):
        return self.employee.last_name + " " + str(self.date) + " " + \
                                                        str(self.daypart)

class Holiday(models.Model):
    class Meta:
        verbose_name = strings.model_holiday_name
        verbose_name_plural = strings.model_holiday_name_plural
    date = models.DateField(primary_key=True)
    def __str__(self):
        return str(self.date)

class Vacation(models.Model):
    class Meta:
        verbose_name = strings.model_vacation_name
    name = models.CharField(strings.model_vacation_name_field, max_length=50,
                                                            unique=True)
    start = models.DateField(strings.model_vacation_start, unique=True)
    end = models.DateField(strings.model_vacation_end, unique=True)
    def __str__(self):
        return str(self.name) + " " + str(self.start) + " - " + str(self.end)

class LeavesAcquired(models.Model):
    class Meta:
        verbose_name = strings.model_leavesacquired_name
        verbose_name_plural = strings.model_leavesacquired_name_plural
        unique_together = ("year", "employee")
    year = models.PositiveSmallIntegerField(strings.model_leavesacquired_year)
    employee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        models.PROTECT,
        verbose_name=strings.model_leavesacquired_employee
    )
    leaves_count = models.FloatField(strings.model_leavesacquired_leaves_count,
                                                                default=32.0)
    rtt_count = models.FloatField(strings.model_leavesacquired_rtt_count,
                                                                default=15.0)
    exceptionals_leaves_count = models.FloatField(
                    strings.model_leavesacquired_ex_leaves_count, default=0.0)
    is_partial_time = models.BooleanField(
                                strings.model_leavesacquired_is_partial_time)
    def __str__(self):
        return str(self.year)

class LeftDate(models.Model):
    class Meta:
        verbose_name = strings.model_leftdate_name
        verbose_name_plural = strings.model_leftdate_name_plural
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        verbose_name=strings.model_leftdate_user
    )
    date = models.DateField(verbose_name=strings.model_leftdate_date)
    def __str__(self):
        return str(self.date)

class JoinedDate(models.Model):
    class Meta:
        verbose_name = strings.model_joineddate_name
        verbose_name_plural = strings.model_joineddate_name_plural
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        verbose_name=strings.model_joineddate_user
    )
    date = models.DateField(verbose_name=strings.model_joineddate_date)
    def __str__(self):
        return str(self.date)

class Setting(models.Model):
    class Meta:
        verbose_name = strings.model_setting_name
    name = models.CharField(strings.model_setting_name, max_length=50,
                                            primary_key=True, editable=False)
    value = models.CharField(strings.model_setting_value, max_length=255)
    def __str__(self):
        return str(self.name)

SUNDAY = 0
MONDAY = 1
TUESDAY = 2
WEDNESDAY = 3
THURSDAY= 4
FRIDAY = 5
SATURDAY = 6
WEEK_DAY_CHOICES = (
    (MONDAY, strings.monday),
    (TUESDAY, strings.tuesday),
    (WEDNESDAY, strings.wednesday),
    (THURSDAY, strings.thursday),
    (FRIDAY, strings.friday),
    (SATURDAY, strings.saturday),
    (SUNDAY, strings.sunday)
)

MORNING = "M"
AFTERNOON = "A"
ENTIRE_DAY = "J"
DAYPART_CHOICES = (
    (MORNING, strings.model_rttsetting_morning),
    (AFTERNOON, strings.model_rttsetting_afternoon),
    (ENTIRE_DAY, strings.model_rttsetting_entire_day)
)

class RTTSetting(models.Model):
    class Meta:
        verbose_name = strings.model_rttsetting_name
        verbose_name_plural = strings.model_rttsetting_name_plural
    employee = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        models.PROTECT,
        verbose_name=strings.model_rttsetting_employee_name,
        primary_key=True
    )
    week_day = models.PositiveSmallIntegerField(
        strings.model_rttsetting_week_day_name,
        choices=WEEK_DAY_CHOICES,
        validators = [
            MaxValueValidator(6)
        ]
    )
    daypart = models.CharField(strings.model_rttsetting_daypart_name,
                                        max_length=1, choices=DAYPART_CHOICES)

class PartialTimeSetting(models.Model):
    class Meta:
        verbose_name=strings.model_partialtimesetting_name
        verbose_name_plural=strings.model_partialtimesetting_name_plural
    employee = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        models.PROTECT,
        verbose_name=strings.model_partialtimesetting_employee_name,
        primary_key=True
    )
    week_day = models.PositiveSmallIntegerField(
        strings.model_partialtimesetting_week_day_name,
        choices=WEEK_DAY_CHOICES,
        validators = [
            MaxValueValidator(6)
        ]
    )

class SubordinateEmployee(models.Model):
    class Meta:
        verbose_name = strings.model_subordinateemployee_name
        verbose_name_plural = strings.model_subordinateemployee_name_plural
        permissions = (
                ("view_statistics", "Peut voir les statistiques des employés"),
                ("allocate_rtt_tp", "Peut affecter les RTT et temps partiels")
        )
    employee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        models.PROTECT,
        verbose_name=strings.model_subordinateemployee_employee,
        related_name="subordinate_set"
    )
    subordinate = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        models.PROTECT,
        verbose_name=strings.model_subordinateemployee_subordinate,
        related_name="supervisor_set"
    )
