from django.contrib.auth.models import Permission
from django.contrib.auth import get_user_model

def remove_print_sheet_perms():
    print("Removing \"planning.print_sheet\" permissions...", end="")
    current_employees = list(get_user_model().objects.exclude(username="root"))
    print_sheet_perm = Permission.objects.get(codename="print_sheet")
    for employee in current_employees:
        employee.user_permissions.remove(print_sheet_perm)
    print(" Done")
