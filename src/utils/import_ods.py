#/*
# * Copyright © 2018 Maël A
# *
# * This file is part of CLEP, a leaves scheduler web app.
# *
# * CLEP is free software: you can redistribute it and/or modify
# * it under the terms of the GNU Affero General Public License as
# * published by the Free Software Foundation, either version 3 of the
# * License, or (at your option) any later version.
# *
# * CLEP is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Affero General Public License for more details.
# *
# * You should have received a copy of the GNU Affero General Public License
# * along with CLEP.  If not, see <https://www.gnu.org/licenses/>.
# */
#
#/*
# * This file and CLEP copy code from and use the Django web framework which is
# * distributed under the following license:
# *
# *
# * Copyright (c) Django Software Foundation and individual contributors.
# * All rights reserved.
# *
# * Redistribution and use in source and binary forms, with or without
# * modification, are permitted provided that the following conditions are met:
# *
# *     1. Redistributions of source code must retain the above copyright
# *        notice, this list of conditions and the following disclaimer.
# *
# *     2. Redistributions in binary form must reproduce the above copyright
# *        notice, this list of conditions and the following disclaimer in the
# *        documentation and/or other materials provided with the distribution.
# *
# *     3. Neither the name of Django nor the names of its contributors may be
# *        used to endorse or promote products derived from this software
# *        without specific prior written permission.
# *
# * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# * POSSIBILITY OF SUCH DAMAGE.
# */

from pyexcel_odsr import get_data
from datetime import datetime, timedelta, date
from django.contrib.auth import get_user_model
from pytz import timezone

from planning import models
from planning.utils.leave_request_view import remove_weekends_and_holidays
from planning.utils.common import leaves_left

from pathlib import Path


from .settings import (
	name_aliases,
	no_pentecost_rtt_employees,
	no_add_rtt_employees
)


def create_name_alias_map(name_aliases):
    name_alias_map = {}
    for alias_pair in name_aliases:
        name_alias_map[alias_pair[0]] = alias_pair[1]
        name_alias_map[alias_pair[1]] = alias_pair[0]
    return name_alias_map

name_alias_map = create_name_alias_map(name_aliases)

months_sheets_names = [
    "JANVIER",
    "FEVRIER",
    "MARS",
    "AVRIL",
    "MAI",
    "JUIN",
    "JUILLET",
    "AOUT",
    "SEPTEMBRE",
    "OCTOBRE",
    "NOVEMBRE",
    "DECEMBRE"
]

employees_sheet_name = "PERSONNEL"
setting_sheet_name = "PARAMS"
requests_log_sheet_name = "SUIVI"

pentecost_mondays = [
    date(2017, 6, 5),
    date(2018, 5, 21)
]

local_tz = timezone("Europe/Paris")
enable_offset = True

def import_ods_to_db(filename):
    if not Path(filename).is_file():
        print(filename, ": file not found ! Skipping data importation.")
        return
    print("Loading the .ods file...", end="", flush=True)
    ods_data = get_data(filename)
    print("Done", flush=True)
    year = int(ods_data[setting_sheet_name][1][0])
    print("Importing holidays...", end="", flush=True)
    import_holidays(ods_data[setting_sheet_name])
    print("Done", flush=True)
    print("Importing vacations...", end="", flush=True)
    import_vacations(ods_data[setting_sheet_name], year)
    print("Done", flush=True)
    print("Importing employees...", end="", flush=True)
    import_employees(ods_data[employees_sheet_name], year)
    print("Done", flush=True)
    print("Creating employee_ref_map...", end="", flush=True)
    employee_ref_map, ref_employee_map = create_employee_ref_maps(
                                                ods_data[employees_sheet_name])
    print("Done", flush=True)
    print("Importing special nonattendances and oncall duties...", end="",
                                                                    flush=True)
    import_nonattendances_oncall_duties(ods_data, months_sheets_names,
                                                    employees_sheet_name, year)
    print("Done", flush=True)
    print("Adding", year,"pentecost monday RTTs...", end="", flush=True)
    add_pentecost_rtt(year)
    print("Done", flush=True)
    print("Importing leaves requests...", end="", flush=True)
    import_requests(ods_data, requests_log_sheet_name, months_sheets_names,
                                                            employee_ref_map)
    print("Done", flush=True)
    print("Checking leaves data consistency...", end="", flush=True)
    crosscheck_calendar(ods_data, months_sheets_names, ref_employee_map, year)
    crosscheck_leaves_count(ods_data[employees_sheet_name], year)
    print("Done", flush=True)

def import_holidays(setting_sheet):
    holidays_start_line = int(setting_sheet[1][1])
    i = holidays_start_line - 1
    while i < len(setting_sheet) and len(setting_sheet[i]) >= 2 and \
                            (type(setting_sheet[i][1]) == date or
                                                len(setting_sheet[i][1]) > 0):
        if type(setting_sheet[i][1]) == date:
            holiday_date = setting_sheet[i][1]
            if holiday_date not in pentecost_mondays:
                holiday = models.Holiday(date=holiday_date)
                holiday.save()
        i += 1

def import_vacations(setting_sheet, year):
    vacations_start_line = int(setting_sheet[1][2])
    i = vacations_start_line - 1
    while i < len(setting_sheet) and len(setting_sheet[i]) >= 3:
        import_vacation_line(setting_sheet[i], year)
        i += 1

def import_vacation_line(sheet_line, year):
    name = sheet_line[1]
    if type(sheet_line[2]) != date:
        start = date(2018, 1 , 1)
        name = name + " - Partie 2"
    else:
        start = sheet_line[2]
    if len(sheet_line) < 4 or type(sheet_line[3]) != date:
        end = date(2018, 12, 31)
        name = name + " - Partie 1"
    else:
        end = sheet_line[3]
    vacation = models.Vacation(name=name, start=start, end=end)
    vacation.save()

def import_employees(employees_sheet, year):
    i = 1
    while i < len(employees_sheet) and len(employees_sheet[i]) >= 18:
        employee_data = parse_employee_line(employees_sheet[i])
        save_employee_data_in_db(employee_data, year)
        i += 1

def parse_employee_line(sheet_line):
    employee_data = {}
    employee_data.update(parse_employee_name(sheet_line[0]))
    employee_data.update(parse_employee_functions(sheet_line[1]))
    add_rtt = employee_data["first_name"] + " " + employee_data["last_name"] \
                                                not in no_add_rtt_employees
    employee_data.update(parse_employee_leaves_acquired(sheet_line, add_rtt))
    employee_data.update(parse_employee_settings(sheet_line))
    employee_data.update(parse_employee_families(sheet_line[17]))
    return employee_data

def parse_employee_name(name):
    splitted_name = name.split()
    raw_first_name = ""
    raw_last_name = ""
    for namepart in splitted_name:
        if namepart.isupper():
            raw_last_name = " ".join((raw_last_name, namepart))
        else:
            raw_first_name = " ".join((raw_first_name, namepart))
    first_name = raw_first_name.strip()
    last_name = raw_last_name.strip()
    return {"first_name": first_name, "last_name": last_name}

def parse_employee_functions(functions_string):
    functions = []
    if functions_string.find("DST") != -1:
        splitted_functions_string = functions_string.split()
        splitted_functions_string.remove("DST")
        functions_string = " ".join(splitted_functions_string)
        functions.append(parse_employee_function_name(functions_string))
        functions.append(parse_employee_function_name(
                                        "directeur des services techniques"))
    elif functions_string.find("/") != -1:
        function_name1, function_name2 = functions_string.split("/")
        functions.append(parse_employee_function_name(function_name1))
        functions.append(parse_employee_function_name(function_name2))
    else:
        functions.append(parse_employee_function_name(functions_string))
    return {"functions": functions}

def parse_employee_function_name(function_name):
    normalized_name = " ".join(function_name.lower().split())
    function_query = models.Function.objects.filter(name=normalized_name)
    if len(function_query) == 1:
        return function_query[0]
    guessed_name = guess_canonical_form(normalized_name)
    function = models.Function.objects.get(name=guessed_name)
    return function

def guess_canonical_form(normalized_name):
    splitted_normalized_name = normalized_name.split()
    end_prefix_list = ["é", "t", "l"]
    for i in range(len(splitted_normalized_name)):
        namepart = splitted_normalized_name[i]
        namepart = namepart.replace("’", "'")
        if len(namepart) > 5 and namepart[-5:] == "trice":
            namepart = namepart[:-5] + "teur"
        elif len(namepart) > 3 and namepart[-3:] == "ère":
            namepart = namepart[:-3] + "er"
        elif namepart == "regie":
            namepart = "régie"
        elif len(namepart) > 2 and namepart != "responsable" and \
                    namepart[-1] == "e" and (namepart[-2] in end_prefix_list):
            namepart = namepart[:-1]
        splitted_normalized_name[i] = namepart
    return " ".join(splitted_normalized_name)

def parse_employee_leaves_acquired(employee_sheet_line, add_rtt):
    previous_year_left = float(employee_sheet_line[2])
    acquired = float(employee_sheet_line[3])
    bonus = float(employee_sheet_line[4])
    if previous_year_left < 0:
        leaves_count = acquired + bonus
    else:
        leaves_count = acquired + bonus - previous_year_left
    fractioned_leaves_count = float(employee_sheet_line[8])
    if employee_sheet_line[12] != "Tp" and employee_sheet_line[12] != "N":
        rtt_count = float(employee_sheet_line[10])
        if add_rtt:
            rtt_count += 1
    else:
        rtt_count = 0
    data = {
        "leaves_count": leaves_count,
        "fractioned_leaves_count": fractioned_leaves_count,
        "rtt_count": rtt_count
    }
    if previous_year_left >= 0:
        data["previous_year_left"] = previous_year_left
    return data

def parse_employee_families(families_str):
    name_map = {
        "A": "administration",
        "D": "direction",
        "T": "technique",
        "R": "régie"
    }
    families = []
    for letter in families_str:
        family_name = name_map[letter]
        families.append(models.Family.objects.get(name=family_name))
    return {"families": families}

def parse_employee_settings(employee_sheet_line):
    if employee_sheet_line[12] == "N":
        return {}
    elif employee_sheet_line[12] == "Tp":
        return parse_employee_partialtime_setting(employee_sheet_line)
    else:
        return parse_employee_rtt_setting(employee_sheet_line)

def parse_employee_partialtime_setting(employee_sheet_line):
    week_day = parse_week_day(employee_sheet_line[14])
    return {"partialtime_week_day": week_day}

def parse_employee_rtt_setting(employee_sheet_line):
    week_day = parse_week_day(employee_sheet_line[14])
    daypart_map = {
        "M": models.MORNING,
        "A": models.AFTERNOON,
        "J": models.ENTIRE_DAY
    }
    daypart = daypart_map[employee_sheet_line[12]]
    return {"rtt_setting": {"week_day": week_day, "daypart": daypart}}

def parse_week_day(date_str):
    date = datetime.strptime(date_str, "%d/%m/%Y").date()
    return (date.weekday() + 1) % 7

def save_employee_data_in_db(employee_data, year):
    user = create_user(employee_data["first_name"], employee_data["last_name"])
    add_functions_to_user(user, employee_data["functions"])
    add_families_to_user(user, employee_data["families"])
    add_leavesacquired_to_user(user, employee_data, year)
    if "rtt_setting" in employee_data:
        add_rtt_setting_to_user(user, employee_data["rtt_setting"])
    elif "partialtime_week_day" in employee_data:
        add_partialtime_setting_to_user(user,
                                        employee_data["partialtime_week_day"])

def create_user(first_name, last_name):
    User = get_user_model()
    username = get_username(first_name, last_name)
    user = User(username=username, first_name=first_name, last_name=last_name)
    user.save()
    return user

def get_username(first_name, last_name):
    sanitized_last_name = last_name.replace(" ", "_").replace("-", "_").lower()
    return first_name[0].lower() + "." + sanitized_last_name

def add_functions_to_user(user, functions):
    for function in functions:
        function_employee_relation = models.FunctionEmployeeRelation(
            function_id=function,
            user_id=user
        )
        function_employee_relation.save()

def add_families_to_user(user, families):
    for family in families:
        family_employee_relation = models.FamilyEmployeeRelation(
            family_id=family,
            user_id=user
        )
        family_employee_relation.save()

def add_leavesacquired_to_user(user, employee_data, year):
    user.leavesacquired_set.create(
        year=year,
        leaves_count=employee_data["leaves_count"],
        rtt_count=employee_data["rtt_count"],
        exceptionals_leaves_count=
                                employee_data["fractioned_leaves_count"],
        is_partial_time=("partialtime_week_day" in employee_data)
    )
    if "previous_year_left" in employee_data:
        user.leavesacquired_set.create(
            year=(year - 1),
            leaves_count=employee_data["previous_year_left"],
            rtt_count=0,
            exceptionals_leaves_count=0,
            is_partial_time=("partialtime_week_day" in employee_data)
        )

def add_rtt_setting_to_user(user, rtt_setting_dict):
    rtt_setting = models.RTTSetting(employee=user, week_day=rtt_setting_dict[
                                        "week_day"], daypart=rtt_setting_dict[
                                                                    "daypart"])
    rtt_setting.save()

def add_partialtime_setting_to_user(user, partialtime_week_day):
    partialtime_setting = models.PartialTimeSetting(employee=user,
                                                week_day=partialtime_week_day)
    partialtime_setting.save()

def import_requests(ods_data, requests_log_sheet_name, months_sheets_names,
                                                            employee_ref_map):
    log_requests_sheet = ods_data[requests_log_sheet_name]
    requests_data = []
    if not enable_offset:
        i = 5
    else:
        i = 3
    while i < len(log_requests_sheet) and len(log_requests_sheet[i]) >= 9:
        requests_data.append(parse_request_line(log_requests_sheet[i]))
        i += 1
    remove_duplicate_requests(requests_data)
    for request_data in requests_data:
        crosscheck_request_data(request_data, ods_data, months_sheets_names,
                                                            employee_ref_map)
        save_request_data_in_db(request_data)

def parse_request_line(sheet_line):
    request_data = {}
    employee_raw_name = sheet_line[0]
    if not employee_exist(employee_raw_name):
        employee_raw_name = name_alias_map[employee_raw_name]
    request_data.update(parse_employee_name(employee_raw_name))
    request_data.update(parse_request_date(sheet_line[1]))
    request_data.update(parse_requested_days(sheet_line[2], sheet_line[3]))
    remove_weekends_and_holidays(request_data["nonattendances"])
    remove_overriding_nonattendances(request_data)
    return request_data

def parse_request_date(date_str):
    date = datetime.strptime(date_str, "%d/%m/%Y // %H:%M:%S")
    localized_date = local_tz.localize(date)
    return {"date": localized_date}

def parse_requested_days(period_str, others_days_str):
    nonattendances = []
    nonattendances.extend(parse_period_str(period_str))
    if len(others_days_str) != 0:
        for day_str in others_days_str.split(";")[1:]:
            nonattendances.extend(parse_day_str(day_str))
    return {"nonattendances": nonattendances}

def parse_period_str(period_str):
    period_str = period_str.replace("–", "-")
    if len(period_str) == 0 or period_str == "00:00:00 - 00:00:00":
        return []
    start_date_str, end_date_str = period_str.split(" - ")
    start_date = parse_fr_fr_date(start_date_str)
    end_date = parse_fr_fr_date(end_date_str)
    nonattendances = []
    date = start_date
    while date <= end_date:
        add_all_dayparts_nonattendances(nonattendances, date)
        date += timedelta(days=1)
    return nonattendances

def parse_day_str(day_str):
    nonattendances = []
    day_str_parts = day_str.split("-")
    if len(day_str_parts) == 1:
        date = parse_fr_fr_date(day_str_parts[0])
        add_all_dayparts_nonattendances(nonattendances, date)
    else:
        date = parse_fr_fr_date(day_str_parts[1])
        daypart_map = {
            "M": models.DayPart.objects.get(name="matin"),
            "A": models.DayPart.objects.get(name="après-midi")
        }
        daypart = daypart_map[day_str_parts[0]]
        nonattendances.append({"date": date, "daypart": daypart})
    return nonattendances

def add_all_dayparts_nonattendances(nonattendances, date):
    for daypart in models.DayPart.objects.all():
        nonattendances.append({"date": date, "daypart": daypart})

def parse_fr_fr_date(date_str):
    return datetime.strptime(date_str, "%d/%m/%Y").date()

def crosscheck_request_data(request_data, ods_data, months_sheets_names,
                                                            employee_ref_map):
    employee_ref = employee_ref_map[request_data["last_name"] + " " +
                                                    request_data["first_name"]]
    for i in range(len(request_data["nonattendances"])):
        crosscheck_nonattendance(request_data, i, ods_data, months_sheets_names,
                                                                employee_ref)

def crosscheck_nonattendance(request_data, i, ods_data, months_sheets_names,
                                                                employee_ref):
    nonattendance = request_data["nonattendances"][i]
    month_sheet = ods_data[months_sheets_names[nonattendance["date"].month - 1]]
    base_line = 5
    while int(month_sheet[base_line][32]) != employee_ref:
        base_line += 2
    if enable_offset:
        day_index = nonattendance["date"].day - nonattendance["daypart"].id
    else:
        day_index = nonattendance["date"].day
    if month_sheet[base_line + nonattendance["daypart"].id][day_index] != "C":
        User = get_user_model()
        employee = User.objects.get(first_name=request_data["first_name"],
                                            last_name=request_data["last_name"])
        if nonattendance["date"] in pentecost_mondays and \
                    employee.leavesacquired_set.get(year=nonattendance[
                                                "date"].year).rtt_count == 0:
            return
        print()
        print("Données incohérentes !", flush=True)
        print("\tEmployé:", request_data["first_name"], end="", flush=True)
        print("", request_data["last_name"], flush=True)
        print("\tDate demande:", request_data["date"], flush=True)
        print("\tJour:", nonattendance, flush=True)

def save_request_data_in_db(request_data):
    User = get_user_model()
    employee = User.objects.get(first_name=request_data["first_name"],
                                            last_name=request_data["last_name"])
    request = models.LeaveRequest()
    request.save()
    request.validated = True
    request.date = request_data["date"]
    request.save()
    leave_type_name = models.Setting.objects.get(name=
                                                "nom_absence_type_conge").value
    leave_type = models.NonattendanceType.objects.get(name=leave_type_name)
    for nonattendance_data in request_data["nonattendances"]:
        nonattendance = models.Nonattendance(request=request, employee=employee,
                date=nonattendance_data["date"], daypart=nonattendance_data[
                                                    "daypart"], type=leave_type)
        nonattendance.save()

def remove_duplicate_requests(requests_data):
    i = 0
    while i < len(requests_data):
        j = i + 1
        while j < len(requests_data):
            if same_requests(requests_data[i], requests_data[j]):
                requests_data.pop(i)
                i -= 1
                break
            j += 1
        i += 1

def same_requests(request_data1, request_data2):
    return request_data1["first_name"] == request_data2["first_name"] \
        and request_data1["last_name"] == request_data2["last_name"] \
        and request_data1["nonattendances"] == request_data2["nonattendances"]

def remove_overriding_nonattendances(request_data):
    User = get_user_model()
    nonattendances = request_data["nonattendances"]
    employee = User.objects.get(first_name=request_data["first_name"],
                                        last_name=request_data["last_name"])
    i = 0
    while i < len(nonattendances):
        nonattendance = nonattendances[i]
        already_na = models.Nonattendance.objects.filter(employee=employee,
                date=nonattendance["date"] , daypart=nonattendance["daypart"])
        if len(already_na) == 1 and already_na[0].type.override_leaves:
            nonattendances.pop(i)
            continue
        i += 1

def import_nonattendances_oncall_duties(ods_data, months_sheets_names,
                                                employees_sheet_name, year):
    employees_data = {}
    for i in range(len(months_sheets_names)):
        month_sheet_name = months_sheets_names[i]
        sheet = ods_data[month_sheet_name]
        new_employees_data = parse_month_sheet(sheet, year, i+1)
        employees_data = assemble_employees_data(employees_data,
                                                            new_employees_data)
    employees_data_list = associate_employees_names_refs(employees_data,
                                                ods_data[employees_sheet_name])
    save_nonattendances_to_db(employees_data_list)
    save_oncall_duties_to_db(employees_data_list, year)

def assemble_employees_data(employees_data, new_employees_data):
    for key in new_employees_data:
        if key not in employees_data:
            employees_data[key] = new_employees_data[key]
            continue
        employees_data[key]["nonattendances"].extend(new_employees_data[key][
                                                            "nonattendances"])
        employees_data[key]["oncall_weeks"].update(new_employees_data[key][
                                                            "oncall_weeks"])
    return employees_data

def parse_month_sheet(sheet, year, month):
    employees_data = {}
    i = 5
    while i < len(sheet) and len(sheet[i]) >= 44:
        employees_data.update(parse_month_sheet_line(sheet, i, year, month))
        i += 2
    return employees_data

def parse_month_sheet_line(sheet, line, year, month):
    employee_ref = int(sheet[line][32])
    nonattendance_type_map = {
        "EM": models.NonattendanceType.objects.get(name="enfant malade"),
        "Ex": models.NonattendanceType.objects.get(name="congé exceptionnel"),
        "F": models.NonattendanceType.objects.get(name="formation"),
        "M": models.NonattendanceType.objects.get(name="maladie"),
        "MAT": models.NonattendanceType.objects.get(name="congé maternité"),
        "P": models.NonattendanceType.objects.get(name="congé paternité"),
        "R": models.NonattendanceType.objects.get(name="RTT"),
        "Tp": models.NonattendanceType.objects.get(name="temps partiel"),
        "Path": models.NonattendanceType.objects.get(name="congé pathologie")
    }
    dayparts = [
        models.DayPart.objects.get(name="matin"),
        models.DayPart.objects.get(name="après-midi")
    ]
    nonattendances = []
    oncall_weeks = set()
    for day_number in range(31):
        for daypart_number in range(2):
            if enable_offset:
                offset = 1 if daypart_number == 0 else 0
                day = offset + day_number + daypart_number
            else:
                offset = 1
                day = offset + day_number
            if offset + day_number >= len(sheet[line + daypart_number]):
                continue
            cell_value = sheet[line + daypart_number][offset + day_number]
            if cell_value not in nonattendance_type_map:
                if cell_value == "A":
                    oncall_weeks.add(week_number(year, month, day))
                if cell_value != "C" and cell_value != "A" and \
                                    cell_value != "" and cell_value != "Sol":
                    print()
                    print("Unknown cell_value found:", cell_value)
                continue
            nonattendance_date = date(year, month, day)
            if nonattendance_date in pentecost_mondays:
                continue
            nonattendances.append({
                "date": nonattendance_date,
                "type": nonattendance_type_map[cell_value],
                "daypart": dayparts[daypart_number]
            })
    return {employee_ref: {
        "nonattendances": nonattendances,
        "oncall_weeks": oncall_weeks
    }}

def week_number(year, month, day):
    day_date = date(year, month, day)
    return int(day_date.strftime("%W"))

def associate_employees_names_refs(employees_data, employees_sheet):
    employees_data_list = []
    for employee_ref in employees_data:
        raw_name = employees_sheet[employee_ref - 1][0]
        employee_data = parse_employee_name(raw_name)
        employee_data["nonattendances"] = employees_data[employee_ref][
                                                            "nonattendances"]
        employee_data["oncall_weeks"] = employees_data[employee_ref][
                                                                "oncall_weeks"]
        employees_data_list.append(employee_data)
    return employees_data_list

def save_nonattendances_to_db(employees_nonattendances_data_list):
    User = get_user_model()
    for employee_na_data in employees_nonattendances_data_list:
        employee = User.objects.get(first_name=employee_na_data["first_name"],
                                        last_name=employee_na_data["last_name"])
        save_employee_nonattendances(employee,
                                            employee_na_data["nonattendances"])

def save_employee_nonattendances(employee, nonattendances):
    if len(nonattendances) == 0:
        return
    request = create_dummy_request()
    save_employee_nonattendance(employee, request, nonattendances[0])
    for i in range(1, len(nonattendances)):
        if not same_group(nonattendances[i - 1], nonattendances[i]):
            request = create_dummy_request()
        save_employee_nonattendance(employee, request, nonattendances[i])

def save_employee_nonattendance(employee, request, nonattendance_data):
    nonattendance = models.Nonattendance(request=request, employee=employee,
                date=nonattendance_data["date"], daypart=nonattendance_data[
                                    "daypart"], type=nonattendance_data["type"])
    nonattendance.save()

def create_dummy_request():
    request = models.LeaveRequest()
    request.save()
    request.validated=True
    request.date = local_tz.localize(datetime(1900, 1 , 1))
    request.save()
    return request

def same_group(nonattendance1, nonattendance2):
    if nonattendance1["type"] != nonattendance2["type"]:
        return False
    last_daypart_id = models.DayPart.objects.count() - 1
    follow = nonattendance2["date"] == nonattendance1["date"] and \
                nonattendance2["daypart"].id == nonattendance1["daypart"].id + 1
    follow = follow or (nonattendance2["date"] == nonattendance1["date"] + \
                timedelta(1) and nonattendance2["daypart"].id == 0 and \
                        nonattendance1["daypart"].id == last_daypart_id)
    follow = follow or (nonattendance2["date"].weekday() == 0 and \
                nonattendance1["date"].weekday() == 4 and \
                    nonattendance2["daypart"].id == 0 and \
                        nonattendance1["daypart"].id == last_daypart_id)
    return follow

def save_oncall_duties_to_db(employees_data_list, year):
    User = get_user_model()
    for employee_data in employees_data_list:
        oncall_duties = employee_data["oncall_weeks"]
        if len(oncall_duties) == 0:
            continue
        employee = User.objects.get(first_name=employee_data["first_name"],
                                        last_name=employee_data["last_name"])
        oncall_employee = models.OnCallEmployee(employee=employee)
        oncall_employee.save()
        for week_number in oncall_duties:
            oncall_week = models.EmployeeOnCallWeek(on_call=oncall_employee,
                                            year=year, week_number=week_number)
            oncall_week.save()

def employee_exist(employee_raw_name):
    User = get_user_model()
    name_pair = parse_employee_name(employee_raw_name)
    match = User.objects.filter(first_name=name_pair["first_name"],
                                    last_name=name_pair["last_name"]).count()
    return match == 1

def create_employee_ref_maps(employees_sheet):
    employee_ref_map = {}
    ref_employee_map = {}
    i = 1
    while i < len(employees_sheet) and len(employees_sheet[i]) >= 18:
        employee_ref_map[employees_sheet[i][0]] = i + 1
        ref_employee_map[i + 1] = employees_sheet[i][0]
        i += 1
    return employee_ref_map, ref_employee_map

def add_pentecost_rtt(year):
    User = get_user_model()
    for pentecost_monday in pentecost_mondays:
        if pentecost_monday.year == year:
            pentecost_date = pentecost_monday
    morning = models.DayPart.objects.get(name="matin")
    afternoon = models.DayPart.objects.get(name="après-midi")
    rtt_type = models.NonattendanceType.objects.get(name="RTT")
    for employee in User.objects.all():
        if employee.first_name + " " + employee.last_name in \
                                                    no_pentecost_rtt_employees:
            continue
        leaves_acquired = employee.leavesacquired_set.get(year=year)
        if leaves_acquired.rtt_count == 0:
            continue
        request = create_dummy_request()
        nonattendance = models.Nonattendance(request=request, employee=employee,
                    date=pentecost_date, daypart=morning, type=rtt_type)
        nonattendance.save()
        nonattendance = models.Nonattendance(request=request, employee=employee,
                    date=pentecost_date, daypart=afternoon, type=rtt_type)
        nonattendance.save()

def crosscheck_calendar(ods_data, months_sheets_names, ref_employee_map, year):
    for i in range(len(months_sheets_names)):
        month_sheet_name = months_sheets_names[i]
        sheet = ods_data[month_sheet_name]
        crosscheck_calendar_month(sheet, year, i + 1, ref_employee_map)

def crosscheck_calendar_month(sheet, year, month, ref_employee_map):
    User = get_user_model()
    i = 5
    while i < len(sheet) and len(sheet[i]) >= 44:
        employee_raw_name = ref_employee_map[int(sheet[i][32])]
        employee_name = parse_employee_name(employee_raw_name)
        employee = User.objects.get(first_name=employee_name["first_name"],
                                        last_name=employee_name["last_name"])
        crosscheck_calendar_month_line(sheet, i, employee, year, month)
        i += 2

def crosscheck_calendar_month_line(sheet, line, employee, year, month):
    leave_type = models.NonattendanceType.objects.get(name="congé")
    for day_number in range(31):
        for daypart_number in range(2):
            if enable_offset:
                offset = 1 if daypart_number == 0 else 0
                day = offset + day_number + daypart_number
            else:
                offset = 1
                day = offset + day_number
            if offset + day_number >= len(sheet[line + daypart_number]):
                continue
            cell_value = sheet[line + daypart_number][offset + day_number]
            if cell_value != "C":
                continue
            nonattendance_date = date(year, month, day)
            daypart = models.DayPart.objects.get(pk=daypart_number)
            req_result = models.Nonattendance.objects.filter(employee=employee,
                        date=nonattendance_date, daypart=daypart)
            if len(req_result) == 1 and req_result[0].type == leave_type:
                continue
            print()
            print("Données incohérentes !", flush=True)
            print("\tEmployé:", employee, flush=True)
            print("\tDate calendrier:", nonattendance_date, flush=True)
            if len(req_result) == 0:
                print("\tAucune absence à cette date dans la BDD !", flush=True)
            else:
                print("\tAbsence dans la BDD:", req_result, flush=True)

def crosscheck_leaves_count(employees_sheet, year):
    User = get_user_model()
    i = 1
    while i < len(employees_sheet) and len(employees_sheet[i]) >= 18:
        employee_name = parse_employee_name(employees_sheet[i][0])
        employee = User.objects.get(first_name=employee_name["first_name"],
                                        last_name=employee_name["last_name"])
        leaves_acquired = float(employees_sheet[i][3]) + \
                                                float(employees_sheet[i][4])
        leaves_used = float(employees_sheet[i][6])
        spares_leaves = float(employees_sheet[i][7])
        crosscheck_employee_leaves_count(employee, year, leaves_acquired,
                                                    leaves_used, spares_leaves)
        i += 1

def crosscheck_employee_leaves_count(employee, year, leaves_acquired,
                                                    leaves_used, spares_leaves):
    leave_type_name = models.Setting.objects.get(
                                            name="nom_absence_type_conge").value
    leave_type = models.NonattendanceType.objects.get(name=leave_type_name)
    start_current_year = date(year, 1, 1)
    acquired_leaves = employee.leavesacquired_set.get(year=year)
    db_leaves_acquired = acquired_leaves.leaves_count \
                                    + acquired_leaves.exceptionals_leaves_count
    try:
        previous_acquired_leaves = employee.leavesacquired_set.get(year=year-1)
        db_leaves_acquired += previous_acquired_leaves.leaves_count \
                            + previous_acquired_leaves.exceptionals_leaves_count
    except models.LeavesAcquired.DoesNotExist:
        pass
    db_leaves_used = models.Nonattendance.objects.filter(employee=employee,
                        type=leave_type, date__gte=start_current_year).count()
    db_leaves_used /= models.DayPart.objects.all().count()
    db_spares_leaves = leaves_left(employee, start_current_year)
    if leaves_acquired != db_leaves_acquired or leaves_used != db_leaves_used \
                                        or spares_leaves != db_spares_leaves:
        print("Données incohérentes !", flush=True)
        print("\tEmployé:", employee.first_name, employee.last_name, flush=True)
        print("\tCongés fichier .ods:", flush=True)
        print("\t\tAcquis:", leaves_acquired, flush=True)
        print("\t\tPris:", leaves_used, flush=True)
        print("\t\tReste:", spares_leaves, flush=True)
        print("\tCongés BDD:", flush=True)
        print("\t\tAcquis:", db_leaves_acquired, flush=True)
        print("\t\tPris:", db_leaves_used, flush=True)
        print("\t\tReste:", db_spares_leaves, flush=True)
