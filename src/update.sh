#!/usr/bin/env bash
#
#/*
# * Copyright © 2018, 2019 Maël A
# *
# * This file is part of CLEP, a leaves scheduler web app.
# *
# * CLEP is free software: you can redistribute it and/or modify
# * it under the terms of the GNU Affero General Public License as
# * published by the Free Software Foundation, either version 3 of the
# * License, or (at your option) any later version.
# *
# * CLEP is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Affero General Public License for more details.
# *
# * You should have received a copy of the GNU Affero General Public License
# * along with CLEP.  If not, see <https://www.gnu.org/licenses/>.
# */

update1() {
	python manage.py loaddata update1.json
	touch UPDATE1
}

update2() {
	#The v1.2 is only a code change, no database or file changes needed
	touch UPDATE2
}

update3() {
	python manage.py loaddata update3.json
	python manage.py shell <<EOF
from utils.removeperms import remove_print_sheet_perms
remove_print_sheet_perms()
EOF
	touch UPDATE3
}

mark_update1() {

python manage.py shell <<EOF
from planning.models import Setting
if Setting.objects.filter(name="afficher_le_numero_des_semaines").count() == 1:
	open("UPDATE1", "a")
EOF

}

mark_update2() {
	#The v1.2 is only a code change, no database or file changes needed
	true
}

mark_update3() {

python manage.py shell <<EOF
from planning.models import Setting
if Setting.objects.filter(name="nom_absence_type_rtt").count() == 1 and \
		Setting.objects.filter(name="nom_absence_type_maladie").count( \
									) == 1:
	open("UPDATE3", "a")
EOF

}


python manage.py migrate

mark_update1
mark_update2
mark_update3

if [ ! -e UPDATE1 ]; then
	update1
fi

if [ ! -e UPDATE2 ]; then
	update2
fi

if [ ! -e UPDATE3 ]; then
	update3
fi

./collectstatic.sh
