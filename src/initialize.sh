#!/usr/bin/env bash
#
#/*
# * Copyright © 2018, 2019 Maël A
# *
# * This file is part of CLEP, a leaves scheduler web app.
# *
# * CLEP is free software: you can redistribute it and/or modify
# * it under the terms of the GNU Affero General Public License as
# * published by the Free Software Foundation, either version 3 of the
# * License, or (at your option) any later version.
# *
# * CLEP is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Affero General Public License for more details.
# *
# * You should have received a copy of the GNU Affero General Public License
# * along with CLEP.  If not, see <https://www.gnu.org/licenses/>.
# */

python manage.py migrate
python manage.py loaddata initial_data.json
python manage.py loaddata update1.json
python manage.py loaddata update3.json

python manage.py shell <<EOF
from utils.import_ods import import_ods_to_db
import_ods_to_db("CONGES - 2018.ods")
EOF

python manage.py createsuperuser

./collectstatic.sh

touch UPDATE1
touch UPDATE2
touch UPDATE3
