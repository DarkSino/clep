#!/usr/bin/env python
#
#/*
# * Copyright © 2018 Maël A
# *
# * This file is part of CLEP, a leaves scheduler web app.
# *
# * CLEP is free software: you can redistribute it and/or modify
# * it under the terms of the GNU Affero General Public License as
# * published by the Free Software Foundation, either version 3 of the
# * License, or (at your option) any later version.
# *
# * CLEP is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Affero General Public License for more details.
# *
# * You should have received a copy of the GNU Affero General Public License
# * along with CLEP.  If not, see <https://www.gnu.org/licenses/>.
# */
#

import wsgiref.handlers
import sys
import os
from pathlib import PurePath

if __name__ == '__main__':
    this_script_dir = PurePath(os.path.abspath(__file__)).parent
    djangoconges_path = PurePath(this_script_dir.parent, "djangoconges" , "src")
    sys.path.append(str(djangoconges_path))
    from main.wsgi import application
    wsgiref.handlers.CGIHandler().run(application)
